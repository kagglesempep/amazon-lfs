from __future__ import print_function
import torch.utils.data as data
from torchvision.datasets.folder import default_loader
from PIL import Image
import pandas as pd
from os.path import join
from functools import partial

def pil_loader(path, rgb=True):
    # open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
    with open(path, 'rb') as f:
        with Image.open(f) as img:
            if rgb:
                return img.convert('RGB')
            else:
                return img

class Amazon(data.Dataset):
    def __init__(self, root, image_type='jpg', train=True, transform=None, loader=None, used_classes=None, argmax=False):
        assert image_type in {'jpg', 'tif'}

        self.transform = transform
        self.train = train

        if loader is None:
            loader = partial(pil_loader, rgb=(image_type=='jpg'))
        
        self.loader = loader

        _imgs_root = ('{}-jpg' if image_type == 'jpg' else '{}-tif-v2').format('train' if train else 'test')
        self.imgs_format = join(root, _imgs_root, '{}.' + image_type)

        if train:
            df = pd.read_csv(join(root, 'train_v2.csv'))
            df = pd.concat([df['image_name'], df.tags.str.get_dummies(sep=' ')], axis=1)
            self.classes = df.columns[1:].tolist() if used_classes is None else used_classes
            self.y = df[self.classes].values.astype('float32')
            self.train_weight = (self.y / self.y.mean(axis=0)).max(axis=1)

            if argmax:
                self.y = self.y.argmax(axis=1)
        else:
            df = pd.read_csv(join(root, 'sample_submission_v2.csv'))
            
        self.df = df

    @staticmethod
    def from_data(df, y, train_weight, other):
        new = Amazon.__new__(Amazon)
        new.df = df
        new.loader = other.loader
        new.train = other.train
        new.imgs_format = other.imgs_format
        new.transform = other.transform
        new.y = y
        new.train_weight = train_weight

        return new
        

    def split_folds(self, splitter, return_index=False):
        for train, valid in splitter.split(self.df):
            amz_train = Amazon.from_data(self.df.iloc[train], self.y[train], self.train_weight[train], self)
            amz_valid = Amazon.from_data(self.df.iloc[valid], self.y[valid], self.train_weight[valid], self)
            
            if return_index:
                yield amz_train, amz_valid, train, valid
            else:
                yield amz_train, amz_valid


    def __getitem__(self, index):
        """
        Args:
            index (int): Index
        Returns:
            tuple: (image, target) where target is class_index of the target class.
        """
        fname = self.df.iloc[index]['image_name']
        img = self.loader(self.imgs_format.format(fname))

        if self.transform is not None:
            img = self.transform(img)

        if self.train:
            target = self.y[index]
            return img, target
        else:
            return img, 0


    def __len__(self):
        return self.df.shape[0]

class JoinDataset(data.Dataset):
    def __init__(self, datasets, constant_target=None):
        self.datasets = datasets
        self.lens = map(len, datasets)
        self.len = sum(self.lens)
        self.constant_target = constant_target

    def __getitem__(self, index):
        ds_idx = index

        for l, ds in zip(self.lens, self.datasets):
            if ds_idx < l:
                if self.constant_target is None:
                    return ds[ds_idx]
                else:
                    return ds[ds_idx][0], self.constant_target
            else:
                ds_idx -= l
        

    def __len__(self):
        return self.len
