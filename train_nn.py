import collections
import weakref
from multiprocessing import Pool
from os.path import exists, join

import cv2
import keras.backend as K
import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
import psutil
from densenet121 import densenet121_model
from keras.applications import VGG16
from keras.applications.imagenet_utils import preprocess_input
#from keras.applications.inception_v3 import preprocess_input
from keras.callbacks import (EarlyStopping, LambdaCallback, ModelCheckpoint,
                             ReduceLROnPlateau, TensorBoard)
from keras.layers import (Activation, BatchNormalization, Conv2D, Dense,
                          Dropout, Flatten, GlobalAveragePooling2D,
                          GlobalMaxPooling2D, Input, MaxPooling2D, concatenate)
from keras.models import Model, load_model
from keras.optimizers import SGD, Adam
from keras.regularizers import l2
from progressbar import ProgressBar
from scipy.optimize import minimize
from skimage.io import imread
from sklearn import metrics
from sklearn.model_selection import KFold
from scale_layer import Scale
import custommodels

from util import LRUCacheDynamicSize, threadsafe_generator, f2_loss, pca_lighting
from savedmodel import SavedModel

train_classes = None #['clear', 'cloudy', 'haze', 'partly_cloudy']

f2_smooth = 1e-4  # Not in use
f2_epsilon = 1e-4 # Not in use
subsample_val = 0.5 # Subsample validation set to speed up train
batch_size = 16  # Batch size
image_float = 'float32' # Image format (it was usefull to save memory in a cache environment)
# pooling = None  # one of None, 'avg' or 'max' (not used anymore)
image_type = 'jpg' # one of 'jpg', 'tif' or 'tif3'
n_folds = 4 # number of folds (4, please!)
reuse_model = None # Load weights from this model and re-train only the dense layers (must check if works))
sm = SavedModel('models/model_0057') # where to save the model
continue_folds = [False, False, False, False] # By default, it a model exists for a fold, it is skiped. Change to True to continue the train on that fold
continue_lr = [None, None, None, None] # Uppon continuing the train, change LR to this value
train_folds = {0, 1, 2, 3} # Which folds to run. It is usefull if you have more than one GPU available
train_densenet = False # Change model to densenet (Keras-based, but not keras.application)
custom_model = False # Change model to custom (Keras-based, but not keras.application)
use_f2_loss = False # Train on f2 loss... did not lead to good results
resample_classes = False # If we should in each batch present at least one sample of each class (must check if is working)
cache_valid = True # if pre-load validation set in memory. may speedup train
use_imagenet_weights = True  # Only valid for Keras applications # Set to False to trian from scrath. Only valid for Keras applications
imagenet_norm = True # Normlization using imagenet parameters (for Keras applications)
use_pcal = False # PCA Lighting augmentation (used in some imagenet papers)
std_multi = 1 # Multiply samples by this factor after normalization (use 60 to put image close to range [-127, 127])
rnd_angle_proba = 0.1 # Probability of arbitrary rotation (note: lossless rotations and flips are always applied)
max_angle = 10 # Maxium angle for arbitrary rotation
max_center_offset_rotate = 10 # Offset from center uppon rotate
l2_weight = 1e-6 # L2 normalization of Dense layers
dropout_p = 0.5 # Dropout
steps_per_epoch = 400 # Number of batchs per epoch
epochs = 200 # Number of epochs (never reached this value)
use_fbw = True

width, height, n_channels = 256, 256, (4 if image_type == 'tif' else 3)

im_shape = (width, height, n_channels) if K.image_data_format() == 'channels_last' else (n_channels, width, height)
dim_axes = (0, 1) if K.image_data_format() == 'channels_last' else (1, 2)

imgs_folder = '../input/train-jpg' if image_type == 'jpg' else '../input/train-tif-v2'

# Leave 1GB free (not in use - has a bogus implementatiom)
num_cached_images = lambda: (psutil.virtual_memory().available - 1 * 2 ** 30) / (width * height * n_channels * (2 if image_float == 'float16' else 4))

# See Data Exploration.ipynb for details
if image_type == 'jpg':
    means_per_channel = np.array([ 77.95814168, 85.57786945, 75.08671441])
    stds_per_channel  = np.array([ 41.65053961, 35.81152416, 34.00985153]) / std_multi
elif image_type == 'tif3':
    means_per_channel = np.array([ 2.67706894e-01, 4.23189691e+03, 6.30784523e-01 ])
    stds_per_channel  = np.array([ 1.50306188e+00,  1.53323986e+03, 2.67391302e+00 ]) / std_multi
else:
    means_per_channel = np.array([ 4947.8677349 , 4231.89690504, 3031.3910505 , 6397.284506  ])
    stds_per_channel  = np.array([ 1682.94966902, 1533.23985562, 1575.38137029, 1840.84254538]) / std_multi

# PCA lighting, tunned on 'PCA Colors.ipynb'
if imagenet_norm:
    pcal_eigen_vector = np.array(
                [[ 0.63631035,  0.55981945,  0.53076485],
                 [ 0.66138937, -0.04174497, -0.74888014],
                 [ 0.3970809 , -0.82756241,  0.39682139]])
    pcal_eigen_value = np.array([ 3405.94453958,    72.00553313,     6.37300645]) 
    pcal_sigma = 0.01
else:
    pcal_eigen_vector = np.array(
                [[ 0.56647001,  0.58186674,  0.58356047],
                 [ 0.6882782 ,  0.05537043, -0.72333065],
                 [ 0.45319405, -0.81139707,  0.3691205 ]])
    pcal_eigen_value = np.array([ 2.59639858,  0.05281296,  0.00470951]) * (std_multi ** 2)
    pcal_sigma = 0.3 / std_multi

# Load image
def load_img(f):
    im = imread(join(imgs_folder, f + '.' + image_type[:3])).astype(np.float32)

    if image_type == 'tif3':
        b, g, r, nir = im[...,0], im[...,1], im[...,2], im[...,3]
        nvdi = (nir-r) / (nir+r)
        gamma = 1
        rb = r - gamma * (b-r)
        arvi = (nir - rb) / (nir + rb)
        im = np.stack([arvi, g, nvdi], axis=2)

    # Simplify process of normalization for jpg/imagenet
    if (image_type == 'jpg') and imagenet_norm:
        im = preprocess_input(im[None])[0]
    else:
        im -= means_per_channel
        im /= stds_per_channel

        if imagenet_norm:
            if n_channels == 4:
                im *= [61.11662676,  60.33262408,  65.21923796, 60.]
                im += [ 124.9789042 ,  122.63242437,  113.3726225, 120. ]
            else:
                im *= [61.11662676,  60.33262408,  65.21923796]
                im += [ 124.9789042 ,  122.63242437,  113.3726225 ]


    if (width != 256) or (height != 256):
        im = cv2.resize(im, (width, height), interpolation=cv2.INTER_AREA)

    if K.image_data_format() == 'channels_first':
        im = np.rollaxis(im, 2)

    return im.astype(image_float)

invalid_tifs = np.unique(pd.read_csv('data/deltraintif.csv', header=None)[0].values)

# Load dataset
df_all = pd.read_csv('../input/train_v2.csv')
df_all = pd.concat([df_all['image_name'], df_all.tags.str.get_dummies(sep=' ')], axis=1)
all_labels = df_all.columns[1:].tolist()

df_fbw = pd.read_csv('data/fbw_train_norm.csv', index_col=0)
assert np.all(df_all['image_name'] == df_fbw['image_name'])
X_fbw = df_fbw.drop('image_name', axis=1).values.astype(K.floatx())

if train_classes is None:
    train_classes = all_labels

n_classes = len(train_classes) if train_classes else 17

y_all = df_all[train_classes].values.astype(K.floatx())

print("Caching %.2f of all data" % (float(num_cached_images()) / df_all.shape[0]))

cached_load_img = load_img #LRUCacheDynamicSize(load_img, num_cached_images, eval_every=batch_size * 50, verbose=False)

def f2_bin_loss(y_true, y_pred):
    return -1 / f2_loss(y_true, y_pred)

def f2_score(y_true, y_pred):
    return -f2_loss(y_true, y_pred)

def random_rotate(im):
    if max_angle > 0:
        center = (width  / 2 + np.random.uniform() * max_center_offset_rotate - max_center_offset_rotate/2,
                  height / 2 + np.random.uniform() * max_center_offset_rotate - max_center_offset_rotate/2)
        rnd_angle = np.random.uniform()*max_angle-max_angle/2.
        M = cv2.getRotationMatrix2D(center, rnd_angle, 1)

        return cv2.warpAffine(im.astype(np.float32), M, (width,height), flags=cv2.INTER_LINEAR, borderMode=cv2.BORDER_REFLECT101)
    else:
        return im

# Define util generators
class gen_xy_sampled:
    def __init__(self, df, x_fbw, y, batch_size=32, rnd_rot=True):
        self.df = df
        self.x_fbw = x_fbw
        self.y = y
        self.rnd_rot = rnd_rot

        if resample_classes:
            # Round batch to number of classes
            batch_size = int(batch_size / float(n_classes) + 0.5) * n_classes
            print("Forcing batch_size={} due to resample_classes".format(batch_size))

            self.idx_per_class = [np.where(y[:,i] == 1)[0] for i in xrange(n_classes)]

        self.batch_size = batch_size

    def __iter__(self):
        return self

    def next(self):
        global cached_load_img

        X_batch = np.empty((self.batch_size,) + im_shape, dtype=K.floatx())

        if resample_classes:
            idx_batch = [np.random.choice(self.idx_per_class[i % n_classes]) for i in xrange(self.batch_size)]
        else:
            idx_batch = np.random.choice(self.df.shape[0], self.batch_size)

        rotate_batch = np.random.randint(0, 4, size=(self.batch_size,))
        rnd_rotate_batch = (np.random.uniform(size=(self.batch_size,)) < rnd_angle_proba) if self.rnd_rot else [False] * self.batch_size
        flip_batch   = np.random.uniform(size=(self.batch_size,)) > 0.5

        y_batch = self.y[idx_batch]
        

        for i, (idx, rotate, rnd_rotate, flip) in enumerate(zip(idx_batch, rotate_batch, rnd_rotate_batch, flip_batch)):
            fname = self.df.iloc[idx]['image_name']
            # Load image
            im = cached_load_img(fname)

            # Apply rotation and flip
            im = np.rot90(im, rotate, axes=dim_axes)
            if flip:
                im = np.flip(im, axis=dim_axes[0])

            if use_pcal:
                im = pca_lighting(im, pcal_sigma, pcal_eigen_value, pcal_eigen_vector)

            X_batch[i] = random_rotate(im) if rnd_rotate else im

        if use_fbw:
            X_batch_fbw = self.x_fbw[idx_batch]

            return {'input': X_batch, 'input-fbw': X_batch_fbw}, y_batch
        else:
            return X_batch, y_batch

def gen_xy(df, y, batch_size=32):
    global cached_load_img

    num = df.shape[0]

    num_batches = num // batch_size
    if num % batch_size != 0:
        num_batches += 1

    @threadsafe_generator
    def local_gen():
        while True:
            for i in xrange(0, num, batch_size):
                this_bacth_size = min(batch_size, num-i)

                batch_images = df.iloc[i:i+batch_size]['image_name']

                X_batch = np.array(map(cached_load_img, batch_images), dtype=K.floatx())
                y_batch = y[i:i+batch_size]

                yield X_batch, y_batch
        
    return local_gen(), num_batches

def transfer_lerning(model_to, model_from):
    common_count = min(len(model_to.layers), len(model_from.layers))
    for i, (l_to, l_from) in enumerate(zip(model_to.layers[:common_count], model_from.layers[:common_count])):
        print("[Layer %02d] Transfering from %s to %s..." % (i, l_to.name, l_from.name))
        if l_to.__class__ != l_from.__class__:
            print("Stop due to incompatible classes!")
            break

        if isinstance(l_to, Model) and isinstance(l_from, Model):
            print("Recursing transfer on layers %s -> %s..." % (l_to.name, l_from.name))
            transfer_lerning(l_to, l_from)
        else:
            l_to.set_weights(l_from.get_weights())

# Start network design
def create_network(prev_model_file):
    if train_densenet:
        model = densenet121_model(img_rows=height, img_cols=width, color_type=n_channels,
                                  num_classes=n_classes, dropout_rate=dropout_p)
    else:
        # Input layer
        input_layer = Input(im_shape, name='input')
    
        if custom_model:
            # Create custom model with only convolutional classes
            x = custommodels.cnn_simple(input_layer, n_blocks=7, n_convs_per_block=2, filter_size=3,
                    activation='relu', base_filters=32, use_batchnorm=True)
        else:
            # Main network application
            if image_type == 'tif':
                # Transfer weigts of layers after first convolution
                x = VGG16(input_tensor=input_layer, weights=None, include_top=False)
        
                if use_imagenet_weights:
                    x_imagenet = VGG16(weights='imagenet', include_top=False)
                                        
                    for xl, vl in zip(x.layers[3:], x_imagenet.layers[3:]):
                        xl.set_weights(vl.get_weights())
        
                    del x_imagenet
        
                x = x(input_layer)
        
            else:
                weights = 'imagenet' if use_imagenet_weights else None
                x = VGG16(input_tensor=input_layer, weights=weights, include_top=False)(input_layer)
        
        # Global max and avg pooling
        #x = GlobalMaxPooling2D()(x)
        x = GlobalAveragePooling2D()(x)
        #x = concatenate([x_max, x_avg], name='global_avg-max')
        #x = Flatten()(x)

        if use_fbw:
            input_fbw = Input((19,), name='input-fbw')
            x = concatenate([x, input_fbw], name='join-fbw')
    
        # Dense layers
        x = Dense(512, name='fc1', kernel_regularizer=l2(l2_weight) if l2_weight > 0 else None)(x)
        #x = BatchNormalization()(x)
        x = Activation('selu')(x)
        x = Dropout(dropout_p)(x)
    
        x = Dense(512, name='fc2', kernel_regularizer=l2(l2_weight) if l2_weight > 0 else None)(x)
    #    x = BatchNormalization()(x)
        x = Activation('selu')(x)
        x = Dropout(dropout_p)(x)
        
        x = Dense(n_classes, activation='sigmoid', name='output')(x)
    
        if use_fbw:
            model = Model([input_layer, input_fbw], x)
        else:
            model = Model(input_layer, x)
    
        if prev_model_file:
            print("Transfering weights from {}...".format(prev_model_file))
            # Load prev model weights as much as we can...
            prev_model = load_model(prev_model_file)
    
            transfer_lerning(model, prev_model)


    optim = SGD(lr=0.001, momentum=0.9, nesterov=False)
    #optim = Adam(lr=0.001)

    model.compile(loss=f2_bin_loss if use_f2_loss else 'binary_crossentropy',
                optimizer=optim,
                metrics=['accuracy', f2_score, 'binary_crossentropy'])

    return model

# Begin cross-validation
kf = KFold(n_folds, random_state=42)

for fold_num, (train_index, val_index) in enumerate(kf.split(df_all)):
    print("Begin fold %d" % fold_num)
    if train_folds and not fold_num in train_folds:
        print("Skiping...")
        continue

    if (subsample_val < 1) and not resample_classes:
        np.random.seed(42)
        val_index = np.random.choice(val_index, int(val_index.shape[0] * subsample_val))

    if image_type == 'tif':
        print("Filtering invalid tifs...")
        train_mask = np.in1d(train_index, invalid_tifs, assume_unique=True)
        train_index = train_index[~train_mask]

        val_mask = np.in1d(val_index, invalid_tifs, assume_unique=True)
        val_index = val_index[~val_mask]

    y_train, y_val = y_all[train_index], y_all[val_index]
    X_fbw_train, X_fbw_val = X_fbw[train_index], X_fbw[val_index]

    # Begin load data
    df_train, df_val = df_all.iloc[train_index], df_all.iloc[val_index]

    gen_train        = gen_xy_sampled(df_train, X_fbw_train, y_train, batch_size=batch_size)

    kval = {}
    if resample_classes or cache_valid:
        gen_val = gen_xy_sampled(df_val, X_fbw_val, y_val, batch_size=batch_size, rnd_rot=False)
        num_val = int(subsample_val * len(y_val) / gen_val.batch_size)
        print("Generating {} batches of validation data due to {}...".format(num_val, 'resample_classs' if resample_classes else 'cache_valid'))

        X_val, X_fbw_val, y_val = [], [], []
        for i in xrange(num_val):
            xv, yv = gen_val.next()
            X_val.append(xv['input'])
            X_fbw_val.append(xv['input-fbw'])
            y_val.append(yv)

        X_val = np.vstack(X_val)
        X_fbw_val = np.vstack(X_fbw_val)
        y_val = np.vstack(y_val)

        del gen_val
        kval['validation_data'] = ({'input': X_val, 'input-fbw': X_fbw_val}, y_val)
    else:
        gen_val, num_val = gen_xy(df_val  , y_val  , batch_size=batch_size)
        kval['validation_data'] = gen_val
        kval['validation_steps'] = num_val


    cache_model_fold = sm("nn_fold_%d.model" % (fold_num))

    exists_prev = exists(cache_model_fold)
    train_fold = (not exists_prev) or (continue_folds[fold_num])

    if train_fold:
        print("Preparing to train...")
        # Create network
        if exists_prev:
            print("Continuing train of fold {}...".format(fold_num))
            model = load_model(cache_model_fold, custom_objects={'f2_loss': f2_loss, 'f2_score': f2_score, 'Scale': Scale})

            if continue_lr[fold_num] is not None:
                print("Changing LR to {}".format(continue_lr[fold_num]))
                K.set_value(model.optimizer.lr, continue_lr[fold_num])

        else:
            prev_model = None if reuse_model is None else join(reuse_model, "nn_fold_%d.model" % fold_num)

            model = create_network(prev_model)

        # Define callbacks
        callbacks = [
            ModelCheckpoint(cache_model_fold, save_best_only=True),
            ReduceLROnPlateau(patience=8, verbose=True, epsilon=0.0005),
            EarlyStopping(patience=16, min_delta=0.0005)
        ]

        if K.backend() == 'tensorflow':
            embeddings_layer_names = ['block1_conv1', 'block7_conv1']
            log_name = sm(join('log', 'fold_{}'.format(fold_num)))
            callbacks.append(TensorBoard(log_dir=log_name, histogram_freq=0, write_graph=False, write_grads=True, embeddings_layer_names=embeddings_layer_names, batch_size=4))
        
        if fold_num == 0:
            print(model.summary())
        
        # Start train

        model.fit_generator(gen_train, steps_per_epoch, epochs=epochs, verbose=1, callbacks=callbacks, workers=3,
            use_multiprocessing=False, max_q_size=50, initial_epoch=0, **kval)

        # Free memory
        del model
    else:
        print("Skiping fold...")
