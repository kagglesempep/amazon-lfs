from __future__ import print_function

import argparse
import inspect
import random
from collections import OrderedDict
from os.path import join

import cv2
import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.backends.cudnn as cudnn
import torch.nn as nn
import torch.nn.init
import torch.nn.parallel
import torch.optim as optim
import torch.utils.data
from torch.autograd import Variable

import torchvision.transforms as transforms
import torchvision.utils as vutils
from amazonds import Amazon, JoinDataset
from savedmodel import SavedModel

parser = argparse.ArgumentParser()
parser.add_argument('--gan_folder', default='./gan/model_0003')
parser.add_argument('--batchSize', type=int, default=32, help='input batch size')
parser.add_argument('--imageSize', type=int, default=64, help='the height / width of the input image to network')
parser.add_argument('--netG', default='./gan/model_0003/netG_epoch_41.pth', help="path to netG (to continue training)")
parser.add_argument('--netD', default='./gan/model_0003/netD_epoch_41.pth', help="path to netD (to continue training)")
parser.add_argument('--cuda', action='store_true', default=True, help='enables cuda')

np.random.seed(42)

opt = parser.parse_args()

nz = 400
ngf = 64
ndf = 64
nc = 3
num_targets = 64
use_features_loss = True

def features_net(x, net, layer_type):
    # Get output for all layer_type

    result = []
    for l in net:
        sb = x.size()
        x = l(x)

        if isinstance(l, layer_type):
            result.append(x.view(-1))

    return torch.cat(result)



# Dynamicaly load networks
with open(join(opt.gan_folder, '_netD.py')) as f:
    exec(''.join(f.readlines()))

with open(join(opt.gan_folder, '_netG.py')) as f:
    exec(''.join(f.readlines()))

netG = _netG(1)
netD = _netD(1)

netG.load_state_dict(torch.load(opt.netG))
netD.load_state_dict(torch.load(opt.netD))

# plt.imshow(netG(Variable(noise.normal_(0, 1).unsqueeze(0)))[0].data.numpy().T)
transform = transforms.Compose([
    transforms.Scale(opt.imageSize),
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
])

amz_train = Amazon('../input', 'jpg', train=True, transform=transform)

while True:
    targets_idx = [342] * num_targets # np.random.choice(len(amz_train), num_targets)

    load_target_idx = lambda idx: amz_train[idx][0].unsqueeze(0)

    target = Variable(torch.cat(map(load_target_idx, targets_idx)))

    criterion = nn.MSELoss()

    # Prepare noise optimization
    noise = torch.FloatTensor(target.size(0), nz, 1, 1).normal_(0, 0.01)

    # From here: https://github.com/zhanghang1989/PyTorch-Style-Transfer/blob/master/experiments/myutils/utils.py#L44-L49
    def gram_matrix(y):
        (b, ch, h, w) = y.size()
        features = y.view(b, ch, w * h)
        features_t = features.transpose(1, 2)
        gram = features.bmm(features_t) / (ch * h * w)
        return gram

    if opt.cuda:
        netD.cuda()
        netG.cuda()
        criterion.cuda()
        noisev = Variable(noise.cuda(), requires_grad=True)
        target = target.cuda()
    else:
        noisev = Variable(noise, requires_grad=True)

    optimizer = optim.Adagrad([noisev], lr=2e-2)

    features_target = Variable(features_net(target, netD.main, nn.LeakyReLU).data, requires_grad=False)


    for i in xrange(10000):
        optimizer.zero_grad()
        output = netG(noisev)

        if use_features_loss:
            output_features = features_net(output, netD.main, nn.LeakyReLU)
        
            err = criterion(output_features, features_target)
        else:
            err = criterion(output, target)

        err.backward()
        
        errv = err.mean().data[0]

        print("%03d loss: %.5f" % (i, errv))
        # if errv < 0.07:
        #     break

        if i % 100 == 0:
            im1 = vutils.make_grid(target.cpu().data, nrow=4).numpy()
            im2 = vutils.make_grid(output.cpu().data, nrow=4).numpy()

            img = np.r_[im1.T, im2.T]
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            cv2.imshow('X', img.clip(-1, 1) * 0.5 + 0.5)
            cv2.waitKey(1)
            # plt.imshow(img.clip(-1, 1) * 0.5 + 0.5)
            # plt.show()

        optimizer.step()
