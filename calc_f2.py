from __future__ import print_function
import numpy as np
from sklearn.metrics import fbeta_score

# From here: https://www.kaggle.com/anokas/planet-understanding-the-amazon-from-space/fixed-f2-score-in-python/code
def f2_score(y_true, y_pred):
    y_true, y_pred, = np.array(y_true), np.array(y_pred)
    return fbeta_score(y_true, y_pred, beta=2, average='samples')


smooth = 1e-4
epsilon = 1e-4
n_classes = 17

y_oof = np.load('./models/model_0011/y_oof.npz')

y_true = y_oof['y_true'] > 0.5
y_pred = y_oof['y_pred'] > 0.5

# Avoid a row with all lines set to zero
y_pred[np.where(((y_pred).sum(axis=1) == 0))[0], 0] = True

score = f2_score(y_true, y_pred)

y_true_eps = np.clip(y_true, epsilon, 1-epsilon)
y_pred_eps = np.clip(y_pred, epsilon, 1-epsilon)

true_and_pred = y_true * y_pred
true_and_pred_eps = y_true_eps * y_pred_eps

tp_sum = true_and_pred.sum(axis=1)
tp_sum_eps = true_and_pred_eps.sum(axis=1)

pred_sum = y_pred.sum(axis=1)
pred_sum_eps = y_pred_eps.sum(axis=1)

true_sum = y_true.sum(axis=1)
true_sum_eps = y_true_eps.sum(axis=1)

precision = tp_sum / pred_sum.astype(np.float)
precision_eps = tp_sum_eps / pred_sum_eps.astype(np.float)

recall = tp_sum / true_sum.astype(np.float)
recall_eps = tp_sum_eps / true_sum_eps.astype(np.float)

beta2 = 2 ** 2

f_score_eps = (((1 + beta2) * precision_eps * recall_eps) / (beta2 * precision_eps + recall_eps))
f_score_smooth = (((1 + beta2) * precision * recall + smooth) / (beta2 * precision + recall + smooth))
f_score_eps_smooth = (((1 + beta2) * precision_eps * recall_eps + smooth) / (beta2 * precision_eps + recall_eps + smooth))

f_score_true = (((1 + beta2) * precision * recall) / (beta2 * precision + recall))
f_score_true[tp_sum == 0] = 0.0

score_calc_eps = f_score_eps.mean()
score_calc_eps_smooth = f_score_eps_smooth.mean()
score_calc_smooth = f_score_smooth.mean()
score_calc_true = f_score_true.mean()

print("score_sklearn:        ", score)
print("score_calc_true:      ", score_calc_true)
print("score_calc_smooth:    ", score_calc_smooth)
print("score_calc_eps:       ", score_calc_eps)
print("score_calc_eps_smooth:", score_calc_eps_smooth)

# Begin keras code, implementing score_calc_eps_smooth
import keras.backend as K

ty_true = K.placeholder(ndim=2, name='y_true')
ty_pred= K.placeholder(ndim=2, name='y_pred')
tbeta2 = K.variable(beta2, name='beta')
tsmooth = K.variable(smooth, name='smooth')

ty_true_eps = K.clip(ty_true, epsilon, 1-epsilon)
ty_pred_eps = K.clip(ty_pred, epsilon, 1-epsilon)

ttrue_and_pred = ty_true_eps * ty_pred_eps
ttp_sum = K.sum(ttrue_and_pred, axis=1)
tpred_sum = K.sum(ty_pred_eps, axis=1)
ttrue_sum = K.sum(ty_true_eps, axis=1)

tprecision = ttp_sum / tpred_sum
trecall = ttp_sum / ttrue_sum


tf_score = ((1 + tbeta2) * tprecision * trecall + tsmooth) / (tbeta2 * tprecision + trecall + tsmooth)

# tf_score2 = tf_score * T.cast(~T.isclose(ttp_sum, 0), 'float32')

f_keras = K.function([ty_true, ty_pred, tbeta], [K.mean(tf_score)])

score_keras = f_keras([y_true.astype(np.float32), y_pred.astype(np.float32)])
print(score, score_calc_eps_smooth, score_keras)
