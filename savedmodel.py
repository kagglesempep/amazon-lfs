import os
import re
import glob

class SavedModel:
    """
    This class will automatically create a new folder for model files
    """
    def __init__(self, folder=None, root_folder='models', verbose=True):
        # Check if we need to create a new folder
        if folder is None:
            if not os.path.exists(root_folder):
                os.mkdir(root_folder)
            
            # Util function to get root_folder id
            folder_id = lambda f: int(next(re.finditer(r'.*model_(\d+)', f)).group(1))

            # Find existing models
            existing_models = sorted(glob.glob(os.path.join(root_folder, 'model_*')))

            # Try to find an empty root_folder
            count_by_folder = map(lambda f: len(glob.glob(os.path.join(f, '*')))-len(glob.glob(os.path.join(f, 'log'))), existing_models)

            if 0 in count_by_folder:
                self.folder = existing_models[count_by_folder.index(0)]
            else:
                # Find the next id (or 1 if none exists)
                my_id = max(map(folder_id, existing_models)) + 1 if len(existing_models) > 0 else 1
                self.folder = os.path.join(root_folder, 'model_%04d' % my_id)

                os.mkdir(self.folder)

        else:
            if not os.path.exists(folder):
                os.mkdir(folder)

            self.folder = folder
        
        if verbose:
            print("Using folder '{}'".format(self.folder))

    def open(self, filename, mode='r'):
        return open(self.path(filename), mode)

    def path(self, filename):
        return os.path.join(self.folder, filename)

    def __call__(self, filename):
        return self.path(filename)
    
    def __str__(self):
        return "SavedModel at '{}'".format(self.folder)

