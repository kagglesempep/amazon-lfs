"""""""""
Pytorch implementation of Conditional Image Synthesis with Auxiliary Classifier GANs (https://arxiv.org/pdf/1610.09585.pdf).
This code is based on Deep Convolutional Generative Adversarial Networks in Pytorch examples : https://github.com/pytorch/examples/tree/master/dcgan
"""""""""
from __future__ import print_function
import argparse
import os
from datetime import datetime
import random
import numpy as np
from progressbar import ProgressBar

import torch
import torch.nn as nn
import torch.nn.parallel
import torch.nn.functional as F
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils
from torch.autograd import Variable

from amazonds import Amazon
from tensorboard_logger import Logger
from sklearn.model_selection import KFold


parser = argparse.ArgumentParser()
parser.add_argument('--dataroot', required=True, help='path to dataset')
parser.add_argument('--predict', action='store_true', help='if run on predict mode')
parser.add_argument('--workers', type=int, help='number of data loading workers', default=2)
parser.add_argument('--batchSize', type=int, default=64, help='input batch size')
parser.add_argument('--imageSize', type=int, default=128, help='the height / width of the input image to network')
parser.add_argument('--nz', type=int, default=100, help='size of the latent z vector')
parser.add_argument('--ngf', type=int, default=64)
parser.add_argument('--ndf', type=int, default=64)
parser.add_argument('--niter', type=int, default=200, help='number of epochs to train for')
parser.add_argument('--lr', type=float, default=0.0002, help='learning rate, default=0.0002')
parser.add_argument('--beta1', type=float, default=0.5, help='beta1 for adam. default=0.5')
parser.add_argument('--cuda', action='store_true', help='enables cuda')
parser.add_argument('--resample', action='store_true', help='if we shoud resample classes')
parser.add_argument('--netG', default='', help="path to netG (to continue training)")
parser.add_argument('--netD', default='', help="path to netD (to continue training)")
parser.add_argument('--outf', default='.', help='folder to output images and model checkpoints')
parser.add_argument('--manualSeed', type=int, help='manual seed')
parser.add_argument('--logf', default='./logs', help="path to logs folder")
parser.add_argument('--folds', default='0123', help="folds to train. detaulf=0123")
parser.add_argument('--alphas', type=float, default=1.0, help='weigth of loss_s')
parser.add_argument('--alphac', type=float, default=1.0, help='weight of loss_c')

opt = parser.parse_args()
print(opt)

try:
    os.makedirs(opt.outf)
except OSError:
    pass

if opt.predict and (opt.netD == ''):
    raise Exception("netD must be specified in predict mode")

if opt.manualSeed is None:
    opt.manualSeed = random.randint(1, 10000)
print("Random Seed: ", opt.manualSeed)
random.seed(opt.manualSeed)
torch.manual_seed(opt.manualSeed)
if opt.cuda:
    torch.cuda.manual_seed_all(opt.manualSeed)

cudnn.benchmark = True

if torch.cuda.is_available() and not opt.cuda:
    print("WARNING: You have a CUDA device, so you should probably run with --cuda")


def RandomRotate(im):
    return im.rotate(random.randint(0, 4) * 90)


if opt.predict:
    # No rotation on predict mode
    transform = transforms.Compose([
        transforms.Scale(opt.imageSize),
        transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
    ])
else:
    transform = transforms.Compose([
        transforms.Scale(opt.imageSize),
        RandomRotate,
        transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
    ])

weather_classes = ['clear', 'cloudy', 'haze', 'partly_cloudy']

dataset = Amazon(opt.dataroot, 'jpg', train=True, transform=transform, used_classes=weather_classes, argmax=True)
dataset_test = Amazon(opt.dataroot, 'jpg', train=False, transform=transform)

print("dataset.y = ", dataset.y.shape)


assert dataset

nz = int(opt.nz)
ngf = int(opt.ngf)
ndf = int(opt.ndf)
nc = 3
nb_label = 4

# custom weights initialization called on netG and netD
def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.01)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.01)
        m.bias.data.fill_(0)


class netG_(nn.Module):

    def __init__(self, nz, ngf, nc):

        super(netG_, self).__init__()
        self.ReLU = nn.ReLU(True)
        self.Tanh = nn.Tanh()

        if opt.imageSize == 128:
            self.conv0 = nn.ConvTranspose2d(nz, ngf * 16, 4, 1, 0, bias=False)
            self.BatchNorm0 = nn.BatchNorm2d(ngf * 16)

            self.conv1 = nn.ConvTranspose2d(ngf * 16, ngf * 8, 4, 2, 1, bias=False)
        else:
            self.conv1 = nn.ConvTranspose2d(nz, ngf * 8, 4, 1, 0, bias=False)
    
        self.BatchNorm1 = nn.BatchNorm2d(ngf * 8)

        self.conv2 = nn.ConvTranspose2d(ngf * 8, ngf * 4, 4, 2, 1, bias=False)
        self.BatchNorm2 = nn.BatchNorm2d(ngf * 4)

        self.conv3 = nn.ConvTranspose2d(ngf * 4, ngf * 2, 4, 2, 1, bias=False)
        self.BatchNorm3 = nn.BatchNorm2d(ngf * 2)

        self.conv4 = nn.ConvTranspose2d(ngf * 2, ngf * 1, 4, 2, 1, bias=False)
        self.BatchNorm4 = nn.BatchNorm2d(ngf * 1)

        self.conv5 = nn.ConvTranspose2d(ngf * 1, nc, 4, 2, 1, bias=False)


        self.apply(weights_init)


    def forward(self, input):

        if opt.imageSize == 128:
            x = self.conv0(input)
            x = self.BatchNorm0(x)
            x = self.ReLU(x)
        else:
            x = input

        x = self.conv1(x)
        x = self.BatchNorm1(x)
        x = self.ReLU(x)

        x = self.conv2(x)
        x = self.BatchNorm2(x)
        x = self.ReLU(x)

        x = self.conv3(x)
        x = self.BatchNorm3(x)
        x = self.ReLU(x)

        x = self.conv4(x)
        x = self.BatchNorm4(x)
        x = self.ReLU(x)

        x = self.conv5(x)
        output = self.Tanh(x)
        return output

    
class netD_(nn.Module):

    def __init__(self, ndf, nc, nb_label):

        super(netD_, self).__init__()
        self.LeakyReLU = nn.LeakyReLU(0.2, inplace=True)

        self.conv1 = nn.Conv2d(nc, ndf, 4, 2, 1, bias=False)

        self.conv2 = nn.Conv2d(ndf     , ndf *  2, 4, 2, 1, bias=False)
        self.BatchNorm2 = nn.BatchNorm2d(ndf *  2)

        self.conv3 = nn.Conv2d(ndf *  2, ndf *  4, 4, 2, 1, bias=False)
        self.BatchNorm3 = nn.BatchNorm2d(ndf *  4)

        self.conv4 = nn.Conv2d(ndf *  4, ndf *  8, 4, 2, 1, bias=False)
        self.BatchNorm4 = nn.BatchNorm2d(ndf *  8)

        if opt.imageSize == 128:
            self.conv5 = nn.Conv2d(ndf *  8, ndf * 16, 4, 2, 1, bias=False)
            self.BatchNorm5 = nn.BatchNorm2d(ndf * 16)

            self.conv6 = nn.Conv2d(ndf * 16, ndf *  1, 4, 1, 0, bias=False)
        else:
            self.conv5 = nn.Conv2d(ndf *  8, ndf  * 1, 4, 1, 0, bias=False)


        self.disc_linear = nn.Linear(ndf * 1, 1)
        self.aux_linear = nn.Linear(ndf * 1, nb_label)
        self.softmax = nn.Softmax()
        self.sigmoid = nn.Sigmoid()
        self.ndf = ndf
        self.apply(weights_init)

    def forward(self, input, out_s, out_c):

        x = self.conv1(input)
        x = self.LeakyReLU(x)

        x = self.conv2(x)
        x = self.BatchNorm2(x)
        x = self.LeakyReLU(x)

        x = self.conv3(x)
        x = self.BatchNorm3(x)
        x = self.LeakyReLU(x)

        x = self.conv4(x)
        x = self.BatchNorm4(x)
        x = self.LeakyReLU(x)

        x = self.conv5(x)

        if opt.imageSize == 128:
            x = self.BatchNorm5(x)
            x = self.LeakyReLU(x)

            x = self.conv6(x)

        x = x.view(-1, self.ndf * 1)

        result = []
        if out_s:
            s = self.disc_linear(x)
            s = self.sigmoid(s)
            result.append(s.view(-1))

        if out_c:
            c = self.aux_linear(x)
            c = self.softmax(c)
            result.append(c)

        if len(result) == 1:
            return result[0]
        else:
            return result


stats = np.zeros((4))

def test(predict, labels):
    correct = 0
    pred = predict.data.max(1)[1].cpu()

    labels_val = labels.data if isinstance(labels, Variable) else labels

    correct = pred.eq(labels_val.cpu()).sum()
    return correct, len(labels_val)


def var(clazz, shape):
    v = clazz(*shape)
    if opt.cuda:
        v = v.cuda()
    return Variable(v)


kf = KFold(4, random_state=42)

ds_folds = dataset.split_folds(kf, return_index=True)

start_time = datetime.today().strftime('%Y%m%d-%H%M%S')

if opt.predict:
    y_oof = np.zeros((len(dataset), 5), dtype=np.float32)
    y_test = np.zeros((4, len(dataset_test), 5), dtype=np.float32)


for fold_num, (ds_train, ds_valid, idx_train, idx_valid) in enumerate(ds_folds):
    print("*" * 30)
    print("Begin fold {}...".format(fold_num))
    print("*" * 30)

    if str(fold_num) not in opt.folds:
        print("> Skipping!")
        continue

    if opt.predict:
        # We only use netD in predict mode
        netD = netD_(ndf, nc, nb_label)

        if opt.cuda:
            netD.cuda()

        input = var(torch.FloatTensor, (opt.batchSize, 3, opt.imageSize, opt.imageSize))

        netD_file = os.path.join(opt.outf, 'fold_{}'.format(fold_num), opt.netD)
        netD.load_state_dict(torch.load(netD_file))

        dataloader_valid = torch.utils.data.DataLoader(ds_valid, batch_size=opt.batchSize, shuffle=False, num_workers=int(opt.workers))

        print("Predicting on fold...")
        start_idx = 0
        bar = ProgressBar()
        for img, label in bar(dataloader_valid):
            batch_size = img.size(0)
            end_idx = start_idx + batch_size

            # c_label.data.resize_(label.size()).copy_(label)
            input.data.resize_(img.size()).copy_(img)

            s_output, c_output = netD(input, True, True)

            cat_output = torch.cat([c_output, s_output.view(-1, 1)], 1)

            y_oof[idx_valid[start_idx:end_idx]] = cat_output.data.cpu().numpy()

            start_idx = end_idx

        print("Predicting on test...")
        start_idx = 0
        dataloader_test = torch.utils.data.DataLoader(dataset_test, batch_size=opt.batchSize, shuffle=False, num_workers=int(opt.workers))
        bar = ProgressBar()
        for img, label in bar(dataloader_test):
            batch_size = img.size(0)
            end_idx = start_idx + batch_size

            # c_label.data.resize_(label.size()).copy_(label)
            input.data.resize_(img.size()).copy_(img)

            s_output, c_output = netD(input, True, True)

            cat_output = torch.cat([c_output, s_output.view(-1, 1)], 1)

            y_test[fold_num, start_idx:end_idx] = cat_output.data.cpu().numpy()

            start_idx = end_idx

    else:
        # Train routine
        log = Logger(os.path.join(opt.logf, datetime.today().strftime('%Y%m%d-%H%M%S'), 'fold_{}'.format(fold_num)), flush_secs=5)

        outf_fold = os.path.join(opt.outf, 'fold_{}'.format(fold_num))
        try:
            os.makedirs(outf_fold)
        except OSError:
            pass

        if opt.resample:
            sampler = torch.utils.data.sampler.WeightedRandomSampler(ds_train.train_weight.astype(float), ds_train.train_weight.shape[0])
            sampler_valid = torch.utils.data.sampler.WeightedRandomSampler(ds_valid.train_weight.astype(float), ds_valid.train_weight.shape[0])
        else:
            sampler = None
            sampler_valid = None

        dataloader = torch.utils.data.DataLoader(ds_train, batch_size=opt.batchSize, sampler=sampler, shuffle=True, num_workers=int(opt.workers))
        dataloader_valid = torch.utils.data.DataLoader(ds_valid, batch_size=opt.batchSize, sampler=sampler_valid, shuffle=True, num_workers=int(opt.workers))
        dataloader_test = torch.utils.data.DataLoader(dataset_test, batch_size=opt.batchSize, shuffle=True, num_workers=int(opt.workers))

        test_iter = dataloader_test.__iter__()
        netG = netG_(nz, ngf, nc)

        if opt.netG != '':
            netG.load_state_dict(torch.load(opt.netG))
        print(netG)

        netD = netD_(ndf, nc, nb_label)

        if opt.netD != '':
            netD.load_state_dict(torch.load(opt.netD))
        print(netD)

        s_criterion = nn.BCELoss()
        c_criterion = nn.NLLLoss()

        input = torch.FloatTensor(opt.batchSize, 3, opt.imageSize, opt.imageSize)
        noise = torch.FloatTensor(opt.batchSize, nz, 1, 1)
        fixed_noise = torch.FloatTensor(opt.batchSize, nz, 1, 1).normal_(0, 1)
        s_label = torch.FloatTensor(opt.batchSize)
        c_label = torch.LongTensor(opt.batchSize)

        real_label = 1
        fake_label = 0

        if opt.cuda:
            netD.cuda()
            netG.cuda()
            s_criterion.cuda()
            c_criterion.cuda()
            input, s_label = input.cuda(), s_label.cuda()
            c_label = c_label.cuda()
            noise, fixed_noise = noise.cuda(), fixed_noise.cuda()

        input = Variable(input)
        s_label = Variable(s_label)
        c_label = Variable(c_label)
        noise = Variable(noise)
        fixed_noise = Variable(fixed_noise)
        fixed_noise_ = np.random.normal(0, 1, (opt.batchSize, nz))
        random_label = np.arange(opt.batchSize) % nb_label# np.random.randint(0, nb_label, opt.batchSize)
        print('fixed label:{}'.format(random_label))
        random_onehot = np.zeros((opt.batchSize, nb_label))
        random_onehot[np.arange(opt.batchSize), random_label] = 1
        fixed_noise_[np.arange(opt.batchSize), :nb_label] = random_onehot[np.arange(opt.batchSize)]


        fixed_noise_ = (torch.from_numpy(fixed_noise_))
        fixed_noise_ = fixed_noise_.resize_(opt.batchSize, nz, 1, 1)
        fixed_noise.data.copy_(fixed_noise_)

        # setup optimizer
        optimizerD = optim.Adam(netD.parameters(), lr=opt.lr, betas=(opt.beta1, 0.999))
        optimizerG = optim.Adam(netG.parameters(), lr=opt.lr, betas=(opt.beta1, 0.999))

        step = -1
        for epoch in range(opt.niter):
            # Begin train step
            netG.train()
            netD.train()

            for i, data in enumerate(dataloader, 0):
                step =  step + 1

                # Sample test data
                data_test = None
                while data_test is None:
                    try:
                        data_test = next(test_iter)
                    except StopIteration:
                        # Restart test iterator
                        test_iter = dataloader_test.__iter__()
                
                img_test, _ = data_test

                ###########################
                # (1) Update D network
                ###########################
                # train with real
                netD.zero_grad()
                img, label = data

                stats += np.bincount(label.numpy(), minlength=4)

                batch_size = img.size(0)
                batch_size_test = img_test.size(0)

                # Train discriminator on single using test data
                input.data.resize_(img_test.size()).copy_(img_test)
                s_output = netD(input, True, False)

                # Train discriminator on classes using train data
                input.data.resize_(img.size()).copy_(img)
                c_output = netD(input, False, True)


                s_label.data.resize_(batch_size_test).fill_(real_label)
                c_label.data.resize_(batch_size).copy_(label)

                s_errD_real = s_criterion(s_output, s_label)
                c_errD_real = c_criterion(c_output, c_label)
                errD_real = opt.alphas * s_errD_real + opt.alphac * c_errD_real
                errD_real.backward()
                D_x = s_output.data.mean()
                
                correct, length = test(c_output, c_label)

                # train with fake
                noise.data.resize_(batch_size, nz, 1, 1)
                noise.data.normal_(0, 1)

                label = np.random.randint(0, nb_label, batch_size)
                noise_ = np.random.normal(0, 1, (batch_size, nz))
                label_onehot = np.zeros((batch_size, nb_label))
                label_onehot[np.arange(batch_size), label] = 1
                noise_[np.arange(batch_size), :nb_label] = label_onehot[np.arange(batch_size)]
                
                noise_ = (torch.from_numpy(noise_))
                noise_ = noise_.resize_(batch_size, nz, 1, 1)
                noise.data.copy_(noise_)

                c_label.data.resize_(batch_size).copy_(torch.from_numpy(label))

                fake = netG(noise)

                s_label.data.resize_(batch_size).fill_(fake_label)
                s_output,c_output = netD(fake.detach(), True, True)
                s_errD_fake = s_criterion(s_output, s_label)
                c_errD_fake = c_criterion(c_output, c_label)
                errD_fake = opt.alphas * s_errD_fake + opt.alphac * c_errD_fake
                errD_fake.backward()
                optimizerD.step()

                D_G_z1 = s_output.data.mean()
                s_errD = s_errD_real + s_errD_fake
                c_errD = c_errD_real + c_errD_fake

                ###########################
                # (2) Update G network
                ###########################
                netG.zero_grad()
                s_label.data.fill_(real_label)  # fake labels are real for generator cost
                s_output,c_output = netD(fake, True, True)
                s_errG = s_criterion(s_output, s_label)
                c_errG = c_criterion(c_output, c_label)
                
                errG = opt.alphas * s_errG + opt.alphac * c_errG
                errG.backward()
                D_G_z2 = s_output.data.mean()
                optimizerG.step()


                accuracy = correct / float(length)

                # Log results so we can see them in TensorBoard after
                log.log_value('s_errD', s_errD.data[0], step)
                log.log_value('c_errD_train', c_errD_real.data[0], step)
                log.log_value('s_errG', s_errG.data[0], step)
                log.log_value('c_errG', c_errG.data[0], step)
                log.log_value('acc', accuracy, step)
            

                print('[%d/%d][%d/%d] Loss_D: %.4f Loss_G: %.4f D(x): %.4f D(G(z)): %.4f / %.4f, acc = %.2f, Labels: %s'
                    % (epoch, opt.niter, i, len(dataloader),
                        s_errD.data[0], s_errG.data[0], D_x, D_G_z1, D_G_z2,
                        accuracy, str(np.round(stats / float(stats.sum()), 2))))
                if i % 200 == 0:
                    if i == 0:
                        vutils.save_image(img,'%s/real_samples.png' % outf_fold, normalize=True)
                    #fake = netG(fixed_cat)
                    fake = netG(fixed_noise)
                    vutils.save_image(fake.data,
                            '%s/fake_samples_epoch_%03d.png' % (outf_fold, epoch),
                            normalize=True)

            print("Evaluating...")
            netD.eval()
            num_valid = 0
            valid_loss = 0.
            valid_acc = 0.
            for i, data in enumerate(dataloader_valid, 0):
                img, label = data

            
                batch_size = img.size(0)

                c_label.data.resize_(label.size()).copy_(label)
                input.data.resize_(img.size()).copy_(img)

                c_output = netD(input, False, True)

                c_errD = c_criterion(c_output, c_label)
                correct, length = test(c_output, c_label)

                valid_loss += batch_size * c_errD.data[0]
                valid_acc  += correct  # it is already multiplied by batch_size
                num_valid  += batch_size

            valid_acc /= num_valid
            valid_loss /= num_valid

            print('valid_acc = %.3f, valid_loss = %.4f' % (valid_acc, valid_loss))
            log.log_value('acc_val', valid_acc, step)
            log.log_value('c_errD_val', valid_loss, step)

            # do checkpointing
            torch.save(netG.state_dict(), '%s/netG_epoch_%d.pth' % (outf_fold, epoch))
            torch.save(netD.state_dict(), '%s/netD_epoch_%d.pth' % (outf_fold, epoch))

if opt.predict:
    np.save(os.path.join(opt.outf, 'y_oof_1pos.npy'), np.expand_dims(y_oof, 1))
    np.save(os.path.join(opt.outf, 'y_test_1pos.npy'), np.expand_dims(y_test, 2))

    with open(os.path.join(opt.outf, 'CLASSES'), 'w') as f:
        all_classes = weather_classes + ['true_or_fake']
        f.write('\n'.join(all_classes))
