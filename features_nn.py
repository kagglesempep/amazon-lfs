from __future__ import print_function

from os.path import join, exists

import numpy as np
import pandas as pd
from progressbar import ProgressBar
from keras.preprocessing import image

try:
    import windows_util
except:
    pass

network = 'vgg19' # one of 'resnet50', 'vgg16' or 'vgg19'
pooling = 'avg'      # one of 'avg' or 'max'
batch_size = 100
width, height = 256, 256

# Definitions
image_format = 'jpg' # one of 'jpg' or 'tif'
input_folder = join('..', 'input')
train_folder = join(input_folder, 'train-' + image_format)
test_folder = join(input_folder, 'test-' + image_format)

# Network name
if network == 'resnet50':
    from keras.applications import ResNet50 as Network
    from keras.applications.resnet50 import preprocess_input
elif network == 'vgg16':
    from keras.applications import VGG16 as Network
    from keras.applications.vgg16 import preprocess_input
elif network == 'vgg19':
    from keras.applications import VGG19 as Network
    from keras.applications.vgg19 import preprocess_input
else:
    raise Exception("Invalid network name: " + network)
print("Using network class {} with {} pooling".format(Network.__name__, pooling))


# Load data
df_train = pd.read_csv(join(input_folder, 'train_v2.csv'))
df_sub = pd.read_csv(join(input_folder, 'sample_submission_v2.csv'))

# One-hot encoding of 'tags'
df_train = pd.concat([df_train['image_name'], df_train.tags.str.get_dummies(sep=' ')], axis=1)
all_labels = df_train.columns[1:].tolist()

model = Network(include_top=False, input_shape=(width, height, 3), pooling=pooling)

# Util functions
def imread(files):
    X = []
    for f in files:
        img = image.load_img(f, target_size=(height, width))
        x = image.img_to_array(img)
        X.append(x)

    X = preprocess_input(np.array(X))

    return X

def imread_folder(images_names, folder):
    files = [join(folder, f + '.jpg') for f in images_names]
    return imread(files)

print("Creating features in train...")
all_y_train = []
bar = ProgressBar()
for i in bar(xrange(0, len(df_train), batch_size)):
    images_names = df_train.iloc[i:i+batch_size]['image_name'].tolist()
    X = imread_folder(images_names, train_folder)

    y = model.predict(X)

    all_y_train.extend(y)

all_y_train = np.array(all_y_train)


print("Creating features in test...")
all_y_test = []
bar = ProgressBar()
for i in bar(xrange(0, len(df_sub), batch_size)):
    images_names = df_sub.iloc[i:i+batch_size]['image_name'].tolist()
    X = imread_folder(images_names, test_folder)

    y = model.predict(X)

    all_y_test.extend(y)

all_y_test = np.array(all_y_test)

np.savez_compressed("data/{}_{}.npz".format(network, pooling), train=all_y_train, test=all_y_test)
