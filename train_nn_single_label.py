import collections
import glob
import weakref
from multiprocessing import Pool
from os.path import exists, join
from datetime import datetime

import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
import psutil
import keras.backend as K
from keras.layers import (Activation, BatchNormalization, Dense, Dropout,
                          Flatten, Input)
from keras.callbacks import (LambdaCallback, ModelCheckpoint, EarlyStopping,
                             ReduceLROnPlateau, TensorBoard)
from keras.models import Model, load_model
from keras.optimizers import SGD
from keras.regularizers import l2
from progressbar import ProgressBar
from scipy.optimize import minimize
from skimage.io import imread
from skimage.transform import downscale_local_mean
from sklearn import metrics
from sklearn.model_selection import KFold
import cv2

from util import f2_score, IntervalEvaluation
from savedmodel import SavedModel


n_folds = 4
n_classes = 17
batch_size = 32
image_float = 'float32'
base_model_folder = 'models/model_0021'
sm = SavedModel('models/model_0049')
validation_augmentaion_times = 16
num_trains_per_val = 2
epochs = 60
max_train_steps_per_epoch = 200
str_start_run = datetime.today().strftime('%Y%m%d-%H%M%S')
train_folds = {2, 3}

labels_to_learn = [
#    'agriculture',
#    "cultivation",
#    'road',
    'water'
]

label_p = 0.5  # Resample binary label to this p

channel_axis = -1 if K.image_data_format() == 'channels_last' else 0
dim_axes = (0, 1) if K.image_data_format() == 'channels_last' else (1, 2)

df_all = pd.read_csv('../input/train_v2.csv')
df_all = pd.concat([df_all['image_name'], df_all.tags.str.get_dummies(sep=' ')], axis=1)
all_labels = df_all.columns[1:].tolist()

print("Loading model from fold 0...")
model0_file = join(base_model_folder, 'nn_fold_0.model')
model0 = load_model(model0_file)

# Get dimensions and image type from input shape
im_shape = model0.layers[0].input_shape[1:]

if K.image_data_format() == 'channels_last':
    width, height, n_channels = im_shape
else:
    n_channels, width, height = im_shape

image_type = 'tif' if n_channels == 4 else 'jpg'

del model0

imgs_folder = '../input/train-jpg' if image_type == 'jpg' else '../input/train-tif-v2'

# Leave 2GB free
num_cached_images = (psutil.virtual_memory().available - 2 * 2 ** 30) / (width * height * n_channels * (2 if image_float == 'float16' else 4))

# See Data Exploration.ipynb for details
if image_type == 'jpg':
    means_per_channel = np.array([ 77.95814168, 85.57786945, 75.08671441])
    stds_per_channel  = np.array([ 41.65053961, 35.81152416, 34.00985153])
else:
    means_per_channel = np.array([ 4947.8677349 , 4231.89690504, 3031.3910505 , 6397.284506  ])
    stds_per_channel  = np.array([ 1682.94966902, 1533.23985562, 1575.38137029, 1840.84254538])

# Load metadata
def load_img(f):
    im = imread(join(imgs_folder, f + '.' + image_type)).astype(image_float)
    #if (image_type == 'jpg') and (im.shape[channel_axis] == 4):
        # print("Fixing 4-channels JPG of file {} ({}, {}, {})".format(f, im.min(axis=dim_axes), im.max(axis=dim_axes), im.mean(axis=dim_axes)))
    #    if K.image_data_format() == 'channels_last':
    #        im = im[...,:3]
    #    else:
    #        im = im[:3]

    im -= means_per_channel
    im /= stds_per_channel

    if (width != 256) or (height != 256):
        im = downscale_local_mean(im, (im.shape[1] // width, im.shape[0] // height, 1))

    if K.image_data_format() == 'channels_first':
        im = np.rollaxis(im, 2)

    return im


# Source: http://stackoverflow.com/a/9692194
cache_stats = dict(all=0, hit=0)
def createLRUCache(factory, maxlen=64):
    global cache_stats
    weak = weakref.WeakValueDictionary()
    strong = collections.deque(maxlen=maxlen)
    print("Creating cache with %d entries" % maxlen)

    notFound = object()
    def fetch(key):
        cache_stats['all'] = cache_stats['all']+1
        value = weak.get(key, notFound)
        if value is notFound:
            weak[key] = value = factory(key)
        else:
            cache_stats['hit'] = cache_stats['hit']+1
        strong.append(value)

        # if cache_stats['all'] % 10000 == 9999:
        #     print("\nCache hit rate: %.4f" % (cache_stats['hit'] / float(cache_stats['all'])))

        return value
    return fetch

cached_load_img = load_img #createLRUCache(load_img, num_cached_images)

def random_rotate(im):
    # return rotate(im, np.random.uniform() * 90 - 45, mode='reflect', center=(width//2 + np.random.normal() * width / 4, height//2 + np.random.normal() * height / 4))
    # return rotate(im.astype(np.float32), np.random.uniform() * 90 - 45, order=1, mode='reflect', reshape=False).astype(image_float)

    center = (width / 2 + np.random.normal() * width / 4, height / 2 + np.random.normal() * height / 4)
    M = cv2.getRotationMatrix2D(center, np.random.uniform()*30-15, 1 )

    return cv2.warpAffine(im.astype(np.float32), M, (width,height), flags=cv2.INTER_LINEAR, borderMode=cv2.BORDER_REFLECT101)

# Define util generators
def gen_xy_sampled(df, y, batch_size=32, seed=None):
    global cached_load_img

    if seed:
        np.random.seed(seed)

    idx0 = np.where(y == 0)[0]
    idx1 = np.where(y == 1)[0]

    while True:
        y_batch = (np.random.uniform(size=(batch_size,)) > label_p).astype(K.floatx())

        idx_batch = [np.random.choice(idx0 if yb == 0 else idx1) for yb in y_batch]

        X_batch = np.empty((batch_size,) + im_shape, dtype=K.floatx())

        rotate_batch = np.random.randint(0, 4, size=(batch_size,))
        flip_batch   = np.random.uniform(size=(batch_size,)) > 0.5
        rnd_rot_batch = np.random.uniform(size=(batch_size,)) > 0.4

        for i, (idx, rotate, rnd_rot, flip) in enumerate(zip(idx_batch, rotate_batch, rnd_rot_batch, flip_batch)):
            fname = df.iloc[idx]['image_name']
            # Load image
            im = cached_load_img(fname)

            # Apply rotation and flip
            im = np.rot90(im, rotate, axes=dim_axes)
            if flip:
                im = np.flip(im, axis=dim_axes[0])

            X_batch[i] = random_rotate(im) if rnd_rot else im

        yield X_batch, y_batch


def gen_xy(df, y, batch_size=32):
    global cached_load_img

    num = df.shape[0]

    num_batches = num // batch_size
    if num % batch_size != 0:
        num_batches += 1

    def local_gen():
        while True:
            for i in xrange(0, num, batch_size):
                this_bacth_size = min(batch_size, num-i)

                batch_images = df.iloc[i:i+batch_size]['image_name']

                X_batch = np.array(map(cached_load_img, batch_images), dtype=K.floatx())
                y_batch = y[i:i+batch_size]

                yield X_batch, y_batch
        
    return local_gen(), num_batches


def rebuild_model(prev_model):
    ldim = lambda l: len(l.get_output_shape_at(0)[1:])
    # Find 'flat' layer
    flat_idx = -1
    for i, l in enumerate(prev_model.layers):
        if ldim(l) == 1:
            flat_idx = i
            break

    flat_layer = prev_model.layers[flat_idx].get_output_at(0)

    if isinstance(prev_model.layers[flat_idx], Model):
        input_layer = prev_model.layers[flat_idx].get_input_at(0)
    else:
        input_layer = prev_model.layers[0].get_input_at(0)

    # Dense layers
    x = Dense(1024, kernel_regularizer=l2(5e-5))(flat_layer)
    #x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = Dropout(0.5)(x)
    x = Dense(1024, kernel_regularizer=l2(5e-5))(x)
    #x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = Dropout(0.5)(x)
    x = Dense(1, activation='sigmoid')(x)

    model = Model(input_layer, x)

    #optim = Adam(lr=0.001)
    optim = SGD(lr=0.0001, momentum=0.9, nesterov=False)

    model.compile(loss='binary_crossentropy',
                optimizer=optim,
                metrics=['accuracy', 'binary_crossentropy'])

    return model
    


print("Loading other models...")
base_models_files = glob.glob(join(base_model_folder, "*.model"))

for labal_idx, label_to_learn in enumerate(labels_to_learn):
    y_all = df_all[label_to_learn].values

    print("=" * 20)
    print("Label to learn: {}".format(label_to_learn))
    print("  resampling from %.5f to %.2f" % (y_all.mean(), label_p))
    print("=" * 20)

    # Begin cross-validation
    kf = KFold(n_folds, random_state=42)

    # y_oof = np.zeros_like(y_all)

    for fold_num, (train_index, val_index) in enumerate(kf.split(df_all, y_all)):
        print("Begin fold %d" % fold_num)
        if not fold_num in train_folds:
            print("Skipping...")
            continue

        y_train, y_val = y_all[train_index], y_all[val_index]

        # Begin load data (or crate generator)
        df_train, df_val = df_all.iloc[train_index], df_all.iloc[val_index]

        gen_train = gen_xy_sampled(df_train, y_train, batch_size=batch_size, seed=41)
        gen_val   = gen_xy_sampled(df_val  , y_val  , batch_size=batch_size, seed=43)

        num_val = int(y_val.sum()) * validation_augmentaion_times // batch_size
        num_train = num_val * num_trains_per_val

        # Clip number ot steps (batch iterations)
        num_train = min(num_train, max_train_steps_per_epoch)
        num_val = min(num_val, max_train_steps_per_epoch // num_trains_per_val)

        print("Generating validation data...")
        Xy_val = [next(gen_val) for i in xrange(num_val)]
        X_val = np.vstack([xy[0] for xy in Xy_val])
        y_val = np.hstack([xy[1] for xy in Xy_val])

        cache_model_fold = sm("nn_%s_fold_%d_{epoch:02d}-{val_roc:.4f}_{val_binary_crossentropy:.4f}_{binary_crossentropy:.4f}.model" % (label_to_learn, fold_num))

        exists_prev = exists(cache_model_fold)
        train_fold = (not exists_prev) or (continue_folds[fold_num])

        if train_fold:
            print("Preparing to train...")
            # Create network
            if exists_prev:
                print("Continuing train of fold {}...".format(fold_num))
                model = load_model(cache_model_fold)

                if continue_lr[fold_num] is not None:
                    print("Changing LR to {}".format(continue_lr[fold_num]))
                    optim = SGD(lr=continue_lr[fold_num], decay=1e-6, momentum=0.9, nesterov=False)

                    model.compile(loss='binary_crossentropy',
                                optimizer=optim,
                                metrics=['accuracy'])

            else:            
                prev_model = load_model(join(base_model_folder, "nn_fold_%d.model" % fold_num))
                model = rebuild_model(prev_model)
                del prev_model

                if labal_idx == 0 and fold_num == 0:
                    print(model.summary())


            # Define callbacks
            callbacks = [
                IntervalEvaluation(validation_data=(X_val, y_val)),
                ModelCheckpoint(cache_model_fold, save_best_only=False),
                ReduceLROnPlateau(patience=3, verbose=True),
                EarlyStopping(patience=5),
            ]

            if K.backend() == 'tensorflow':
                callbacks.append(TensorBoard(log_dir=sm(join('log', label_to_learn + '_fold_' + str(fold_num))), histogram_freq=0, write_graph=False))

        else:
            print("Loading model from cache...")
            model = load_model(cache_model_fold)

        # Start train
        if train_fold:
            model.fit_generator(gen_train, num_train, epochs=epochs, verbose=1, callbacks=callbacks, workers=1,
                validation_data=(X_val, y_val), pickle_safe=False)

        # Reset generator for oof generation
        # print("Generating OOF...")
        # gen_val, num_val = gen_xy(df_val, y_val, batch_size=batch_size)

        # y_oof[val_index] = model.predict_generator(gen_val, num_val)

        # Free memory
        del model, X_val, y_val

    # np.savez_compressed(sm('y_oof_{}.npz'.format(label_to_learn)), y_true=y_all, y_pred=y_oof)
