from __future__ import print_function

import glob
import re
import sys
from collections import Counter
from itertools import compress
from multiprocessing import Pool, cpu_count
from os.path import exists, join, split

import lightgbm as lgb
import numpy as np
import pandas as pd
from scipy import stats
from scipy.optimize import minimize
from sklearn.feature_selection import VarianceThreshold
from sklearn.metrics import fbeta_score
from sklearn.model_selection import KFold, StratifiedKFold

from lgbcv import cv
from util import XGBCVHolder, f2_score
from itertools import chain

n_folds = 4
input_folder = join('..', 'input')
use_extra = False
tree_cuts = [0.5, 0.55, 0.6, 0.65, 0.7, 0.8, 0.9, 1.0]
n_classes = 17
only_related_labels = True
used_aggs = {'std'}  # Best until now uses only std

all_labels = ['agriculture', 'artisinal_mine', 'bare_ground', 'blooming',
       'blow_down', 'clear', 'cloudy', 'conventional_mine', 'cultivation',
       'habitation', 'haze', 'partly_cloudy', 'primary', 'road',
       'selective_logging', 'slash_burn', 'water']


only_label_preds = {'artisinal_mine', 'bare_ground', 'clear',
    'cloudy', 'conventional_mine', 'cultivation', 'habitation',
    'haze', 'partly_cloudy', 'primary', 'road', 'slash_burn',
    'water'}

related_labels = {
    'agriculture': ['bare_ground', 'cultivation'],
    'artisinal_mine': [],
    'bare_ground': ['agriculture', 'artisinal_mine', 'partly_cloudy'],
    'blooming': ['selective_logging', 'blow_down'],
    'blow_down': all_labels,
    'clear': ['partly_cloudy', 'haze', 'cloudy'],
    'cloudy': [],
    'conventional_mine': [],
    'cultivation': ['agriculture', 'conventional_mine'],
    'habitation': [],
    'haze': [],
    'partly_cloudy': [],
    'primary':[],
    'road': [],
    'selective_logging': all_labels,
    'slash_burn': ['blow_down', 'artisinal_mine', 'bare_ground'],
    'water': []
}

# related_labels = {
#     'agriculture': ['bare_ground', 'cultivation'],
#     'artisinal_mine': ['bare_ground', 'conventional_mine', 'water'],
#     'bare_ground': ['agriculture', 'artisinal_mine', 'partly_cloudy'],
#     'blooming': ['selective_logging', 'blow_down'],
#     'blow_down': ['cultivation', 'selective_logging'],
#     'clear': ['partly_cloudy', 'haze', 'cloudy'],
#     'cloudy': ['primary', 'conventional_mine', 'road'],
#     'conventional_mine': ['artisinal_mine', 'bare_ground'],
#     'cultivation': ['agriculture', 'conventional_mine'],
#     'habitation': ['conventional_mine',  'slash_burn', 'blow_down'],
#     'haze': ['clear' 'partly_cloudy', 'bare_ground'],
#     'partly_cloudy': ['clear', 'artisinal_mine', 'water'],
#     'primary' :['cultivation', 'cloudy', 'road'],
#     'road': ['water'],
#     'selective_logging': ['blooming', 'water', 'haze'],
#     'slash_burn': ['blow_down', 'artisinal_mine', 'bare_ground'],
#     'water': ['road']
# }

# if len(sys.argv) < 3:
#     print("Please specify models folders as parameter")
#     sys.exit(1)

#models = sys.argv[1:]

# models_num = [7, 10, 11, 13, 14, 15, 18, 19, 21, 24, 26, 30, 36, 38, 39, 40, 50, 51, 52, 53]
models_num = [11, 14, 19, 21, 26, 30, 36, 39, 40, 50, 51, 52, 53]  # Most relevant models

models = map(lambda n: 'models/model_%04d' % n, models_num)

for m in models:
    if not exists(m):
        print("Folder '{}' does not exists".format(m))
        sys.exit(2)

# all_labels = ['clear', 'cloudy', 'haze', 'partly_cloudy']

df_train = pd.read_csv('../input/train_v2.csv')
df_train = pd.concat([df_train['image_name'], df_train.tags.str.get_dummies(sep=' ')], axis=1)
y_true = df_train[all_labels].values.astype('float')


get_model_csv = lambda f: glob.glob(join(f, 'model_*.csv.gz'))[0]
get_fname = lambda f: split(f)[-1]

df_sub = pd.read_csv(join(input_folder, 'sample_submission_v2.csv'))

y_pred = []
y_sub = []

y_pred_all_by_class = {k:[] for k in all_labels}
y_sub_all_by_class = {k:[] for k in all_labels}

cols_names = []

mean_per_model = {}

for m in models:
    print("Loading model {}...".format(m))

    y_oof_fnames = set(map(get_fname, glob.glob(join(m, 'y_oof*.*'))))

    # Remove 'old' format if 'new' is present
    if 'y_oof.npz' in y_oof_fnames and 'y_oof_8pos.npy' in y_oof_fnames:
        y_oof_fnames -= {'y_oof.npz'}

    if len(y_oof_fnames) == 0:
        raise Exception("No files for this model")

    for y_oof_fname in y_oof_fnames:
        # 'Old' format
        y_oof_file = join(m, y_oof_fname)
        if y_oof_fname == 'y_oof.npz':
            y_oof = np.load(y_oof_file)

            mean_per_model[y_oof_file] = y_oof['y_pred']

            y_pred.append(y_oof['y_pred'])
            y_sub.append(pd.read_csv(get_model_csv(m), usecols=all_labels).values)

            cols_names.extend([y_oof_file + '_' + l + '_1pos_single' for l in all_labels])

        else:
            print("> Using 8pos data")
            y_oof_8pos = np.load(y_oof_file)
            y_test_8pos = np.load(y_oof_file.replace('oof', 'test'))

            if exists(join(m, 'CLASSES')):
                print("> Loading classes file")
                with open(join(m, 'CLASSES')) as f:
                    m_labels = f.readlines()
                    m_labels = map(str.strip, m_labels)

                num_labels = len(m_labels)
            elif y_oof_8pos.shape[-1] == 1:
                label_name = re.findall(r'y_oof_8pos_([\w_]+)\.npy', y_oof_fname)[0]
                m_labels = [label_name]
                num_labels = -1
            else:
                num_labels = n_classes
                m_labels = all_labels

            if y_oof_8pos.shape[-1] == num_labels:
                cols_names.extend([y_oof_file + '_' + l + '_8pos_mean' for l in m_labels])
                if 'std' in used_aggs:
                    cols_names.extend([y_oof_file + '_' + l + '_8pos_std' for l in m_labels])
                # cols_names.extend([y_oof_file + '_' + l + '_8pos_kurtosis' for l in m_labels])
                # cols_names.extend([y_oof_file + '_' + l + '_8pos_skew' for l in m_labels])
                if 'min' in used_aggs:
                    cols_names.extend([y_oof_file + '_' + l + '_8pos_min' for l in m_labels])
                if 'max' in used_aggs:
                    cols_names.extend([y_oof_file + '_' + l + '_8pos_max' for l in m_labels])
                # cols_names.extend([y_oof_file + '_' + l + '_8pos_minmax_rel' for l in m_labels])
                mean_per_model[y_oof_file] = y_oof_8pos.mean(axis=-2)
            else:
                cols_names.append(y_oof_file + '_' + label_name + '_focus_8pos_mean')
                if 'std' in used_aggs:
                    cols_names.append(y_oof_file + '_' + label_name + '_focus_8pos_std')
                # cols_names.append(y_oof_file + '_' + label_name + '_focus_8pos_kurtosis')
                # cols_names.append(y_oof_file + '_' + label_name + '_focus_8pos_skew')
                if 'min' in used_aggs:
                    cols_names.append(y_oof_file + '_' + label_name + '_focus_8pos_min')
                if 'max' in used_aggs:
                    cols_names.append(y_oof_file + '_' + label_name + '_focus_8pos_max')
                # cols_names.append(y_oof_file + '_' + label_name + '_focus_8pos_minmax_rel')

            for i, l in enumerate(m_labels):
                y_pred_all_by_class[l].append(y_oof_8pos[...,i])

                y_sub_all_by_class[l].append(y_test_8pos[...,i].mean(axis=0))
                

            # For train, we have shape of (40479, 8, c), where c is the number of classes
            y_pred.append(y_oof_8pos.mean(axis=-2))
            if 'std' in used_aggs:
                y_pred.append(y_oof_8pos.std(axis=-2))
            # y_pred.append(stats.kurtosis(y_oof_8pos, axis=-2))
            # y_pred.append(stats.skew(y_oof_8pos, axis=-2))
            if 'min' in used_aggs:
                y_pred.append(y_oof_8pos.min(axis=-2))
            if 'max' in used_aggs:
                y_pred.append(y_oof_8pos.max(axis=-2))
            # y_pred.append(y_oof_8pos.min(axis=-2)/y_oof_8pos.max(axis=-2).clip(1e-8, 1))

            # For submission, we have shape (3, 61191, 8, c), that includes the ouput of three folds
            y_sub.append(y_test_8pos.mean(axis=(0,-2)))
            if 'std' in used_aggs:
                y_sub.append(y_test_8pos.std(axis=-2).mean(axis=0))
            # y_sub.append(stats.kurtosis(y_test_8pos, axis=-2).mean(axis=0))
            # y_sub.append(stats.skew(y_test_8pos, axis=-2).mean(axis=0))
            if 'min' in used_aggs:
                y_sub.append(y_test_8pos.min(axis=-2).mean(axis=0))
            if 'max' in used_aggs:
                y_sub.append(y_test_8pos.max(axis=-2).mean(axis=0))
            # y_sub.append(y_test_8pos.min(axis=-2).mean(axis=0)/y_test_8pos.max(axis=-2).mean(axis=0).clip(1e-8, 1))

# Create overall by label
no_none = lambda x: x is not None
model_join_rtol = 1.
print("Creating overall by label...")
for idx, label in enumerate(all_labels):
    models_mean = np.array(map(np.mean, y_pred_all_by_class[label]))
    
    used_models = np.isclose(models_mean, y_true[:, idx].mean(), model_join_rtol)

    print("> Using {} of {} for label {}".format(used_models.sum(), used_models.shape[0], label))

    y_pred_label = np.hstack(compress(y_pred_all_by_class[label], used_models))
    y_sub_label = np.hstack(compress(y_sub_all_by_class[label], used_models))

    y_pred_label_join = np.stack(filter(no_none, [
            y_pred_label.mean(axis=1),
            y_pred_label.std(axis=1) if 'std' in used_aggs else None,
            # stats.kurtosis(y_pred_label, axis=1),
            # stats.skew(y_pred_label, axis=1),
            y_pred_label.min(axis=1) if 'min' in used_aggs else None,
            y_pred_label.max(axis=1) if 'max' in used_aggs else None,
            # y_pred_label.min(axis=1) / y_pred_label.max(axis=1).clip(1e-8, 1),
        ]), axis=1)

    y_sub_label_join = np.stack(filter(no_none, [
            y_sub_label.mean(axis=1),
            y_sub_label.std(axis=1) if 'std' in used_aggs else None,
            # stats.kurtosis(y_sub_label, axis=1),
            # stats.skew(y_sub_label, axis=1),
            y_sub_label.min(axis=1) if 'min' in used_aggs else None,
            y_sub_label.max(axis=1) if 'max' in used_aggs else None,
            # y_sub_label.min(axis=1) / y_sub_label.max(axis=1).clip(1e-8, 1),
        ]), axis=1)

    y_pred.append(y_pred_label_join)
    y_sub.append(y_sub_label_join)

    agg_funcs = ['mean'] + filter(lambda f: f in used_aggs, ['std', 'min', 'max'])
    cols_names.extend(map(lambda agg: 'models/model_0000/y_oof_8pos.npy_' + label + '_8pos_' + agg, agg_funcs))


# Create overall by label
print("Creating overall norm by label...")
for label in all_labels:
    y_pred_label = np.hstack(y_pred_all_by_class[label])
    y_sub_label = np.hstack(y_sub_all_by_class[label])

    y_sub_label -= y_pred_label.mean(axis=0)
    y_sub_label /= y_pred_label.std(axis=0)

    y_pred_label -= y_pred_label.mean(axis=0)
    y_pred_label /= y_pred_label.std(axis=0)

    y_pred_label_join = np.stack(filter(no_none, [
            y_pred_label.mean(axis=1),
            y_pred_label.std(axis=1) if 'std' in used_aggs else None,
            # stats.kurtosis(y_pred_label, axis=1),
            # stats.skew(y_pred_label, axis=1),
            y_pred_label.min(axis=1) if 'min' in used_aggs else None,
            y_pred_label.max(axis=1) if 'max' in used_aggs else None,
            # y_pred_label.min(axis=1) / y_pred_label.max(axis=1).clip(1e-8, 1),
        ]), axis=1)

    y_sub_label_join = np.stack(filter(no_none, [
            y_sub_label.mean(axis=1),
            y_sub_label.std(axis=1) if 'std' in used_aggs else None,
            # stats.kurtosis(y_sub_label, axis=1),
            # stats.skew(y_sub_label, axis=1),
            y_sub_label.min(axis=1) if 'min' in used_aggs else None,
            y_sub_label.max(axis=1) if 'max' in used_aggs else None,
            # y_sub_label.min(axis=1) / y_sub_label.max(axis=1).clip(1e-8, 1),
        ]), axis=1)


    y_pred.append(y_pred_label_join)
    y_sub.append(y_sub_label_join)

    agg_funcs = ['mean'] + filter(lambda f: f in used_aggs, ['std', 'min', 'max'])
    cols_names.extend(map(lambda agg: 'models/model_0001/y_oof_8pos.npy_' + label + '_norm_8pos_' + agg, agg_funcs))


# Stack y_pred into X_train and y_sub into X_test
X_train = np.hstack(y_pred)
X_test = np.hstack(y_sub)

assert np.isnan(X_train).sum() == 0
assert np.isnan(X_test).sum() == 0

# Free memory
del y_pred_all_by_class, y_sub_all_by_class, y_pred, y_sub

assert X_test.shape[0] == df_sub.shape[0], "Test set not in sync"
assert X_train.shape[1] == len(cols_names), "Cols name not in sync"

model_name = lambda f: re.findall(r'/model_0*(\d+)/', f)[0]
model_label = lambda f: re.findall(r'\.np._(.*)_\dpos_.*', f)[0]
model_agg = lambda f: re.findall(r'\.np._.*_\dpos_(.*)', f)[0]
cols_idx_label = lambda l: [i for i, c in enumerate(cols_names) if model_label(c) in {l, l+'_norm', l+'_focus', l+'_single'}]

# fig, ax = plt.subplots(4, 5)
# ax = ax.ravel()

# means_cols = [c for c in cols_names if '_mean' in c]

# is_mean_col = lambda c, l: c.endswith('npy_' + l + '_8pos_mean') or c.endswith('npy_' + l + '_focus_8pos_mean')

# for i, label in enumerate(all_labels):
#     mean_cols_mask = map(lambda c: is_mean_col(c, label), cols_names)
#     mean_cols_idx = np.where(mean_cols_mask)[0]
#     mean_cols_names = [model_name(cols_names[ci]) for ci in mean_cols_idx]

#     X_corr_means = np.corrcoef(X_train[:,mean_cols_idx], rowvar=False)

#     ax[i].matshow(X_corr_means, vmin=-1, vmax=1, extent=[0, X_corr_means.shape[0], 0, X_corr_means.shape[0]], cmap='jet')
#     ax[i].set_xticklabels(['']+mean_cols_names)
#     ax[i].set_yticklabels(['']+mean_cols_names[::-1])

#     ax[i].set_title(label)



# def corr_label(idx):
#     label = all_labels[idx]

#     values = np.hstack([v[:,idx].reshape(-1, 1) for v in mean_per_model.values()])

#     model_name = lambda m: re.findall(r'/(model_\d+)', m)[0]

#     df_result = pd.DataFrame(columns=map(model_name, mean_per_model.keys()), data=values)

#     return df_result.corr()


if use_extra:
    X_train = np.hstack([X_train, np.load('X_train_hist_016.npy')])
    X_test = np.hstack([X_test, np.load('X_test_hist_016.npy')])


# Start train-predict for each label

# LightGBM hyperparameters
num_leaves_opts = [7, 15]
lgb_params = {
    'application': 'binary',
    'boosting': 'dart',  # 'dart' or 'gbdt'
    'learning_rate': 0.05,
    'min_data_in_leaf': 100,
    'metric': 'binary_logloss',
    'max_bin': 255,
    'verbosity': 1,
    'feature_fraction': 0.8,
    'bagging_fraction': 0.8,
    'verbose': 1,
    'num_threads': cpu_count()
}

dtest_all = lgb.Dataset(X_test, feature_name=cols_names)

y_oof = {k:[] for k in tree_cuts}
df_sub_tree_cut = {k:df_sub.copy() for k in tree_cuts}
y_test = []
df_results = pd.DataFrame(columns=['col', 'num_leaves', 'num_boost_round', 'binary_logloss', 'score_per_num_leaves'])
all_histories = {l:[] for l in all_labels}

def print_most_common(counter, top=5):
    pd.options.display.max_colwidth = 128
    df = pd.DataFrame(counter.most_common(top), columns=['name', 'value'])
    print(df)

labels_fscores = {}

all_models_labels = sorted(set(map(model_label, cols_names)))

df_model_importance = pd.DataFrame(index=all_labels, columns=map(str, models_num))
df_label_importance = pd.DataFrame(index=all_labels, columns=all_models_labels)

for idx, label in enumerate(all_labels):
    print()
    print('*' * 50)
    print("Tunning label '{}'...".format(label))

    if only_related_labels:
        used_labels = [label] + related_labels[label]
        print("> Using labels: {}".format(", ".join(used_labels)))

        cols_idx = sorted(set(chain(*map(cols_idx_label, used_labels))))

        cols_names_label = map(cols_names.__getitem__, cols_idx)
        X_train_label = X_train[:, cols_idx]
        X_test_label = X_test[:, cols_idx]
        dtest = lgb.Dataset(X_test_label, feature_name=cols_names)

        assert len(cols_names_label) == X_train_label.shape[1], "Invalid cols names"

    elif label in only_label_preds:
        print("> Using label-specific columns")
        cols_idx = cols_idx_label(label)
        cols_names_label = map(cols_names.__getitem__, cols_idx)
        X_train_label = X_train[:, cols_idx]
        X_test_label = X_test[:, cols_idx]
        dtest = lgb.Dataset(X_test_label, feature_name=cols_names)

        assert len(cols_names_label) == X_train_label.shape[1], "Invalid cols names"
    else:
        cols_names_label = cols_names
        X_train_label = X_train
        X_test_label = X_test
        dtest = dtest_all

    # Perform cross-validation on train data
    best_score = 0
    best_num_leaves = -1
    best_num_boost_round = -1
    best_xgb_model = None
    score_per_num_leaves = {}
    folds = list(KFold(n_folds, random_state=42).split(X_train_label))
    for num_leaves in num_leaves_opts:
        print("> Trying {} leaves".format(num_leaves))
        score, n_rounds, xgb_model, history = cv(dict(lgb_params, num_leaves=num_leaves), X_train_label, y_true[:, idx], folds,
                num_boost_round=2000, early_stopping_rounds=20, verbose_eval=20, features_names=cols_names_label, maximize=True)

        score_per_num_leaves[num_leaves] = score
        all_histories[label].append(history)


        if score > best_score:
            best_score = score
            best_num_leaves = num_leaves
            best_num_boost_round = n_rounds
            best_xgb_model = xgb_model

    print(best_score)
    df_results.loc[len(df_results)] = {
        'col': label,
        'num_leaves': best_num_leaves,
        'num_boost_round': best_num_boost_round,
        'binary_logloss': best_score,
        'score_per_num_leaves': score_per_num_leaves
    }

    best_features = best_xgb_model.get_fscore()
    model_count = Counter()
    label_count = Counter()
    agg_count = Counter()
    for k, v in best_features.iteritems():
        model_count.update({model_name(k): v})
        label_count.update({model_label(k): v})
        agg_count.update({model_agg(k): v})

    df_model_importance.loc[label] = model_count
    df_label_importance.loc[label] = label_count
    
    print("\nMost common FEATURES:")
    print_most_common(best_features, 10)
    print("\nMost common MODELS:")
    print_most_common(model_count, 5)
    print("\nMost common LABELS:")
    print_most_common(label_count, 5)
    print("\nMost common AGG:")
    print_most_common(agg_count, 5)

    labels_fscores[label] = best_features

    # Get OOF predictions based on models trained on CV
    print("Evaluating...")
    for tree_cut in tree_cuts:
        cut_rounds = 1 + int(tree_cut * best_num_boost_round)
        y_oof[tree_cut].append(best_xgb_model.predict_oof(ntree_limit=cut_rounds)[1])

        # Train main model
        df_sub_tree_cut[tree_cut][label], df_sub_tree_cut[tree_cut][label + '_std'] = best_xgb_model.predict_mean(dtest, ntree_limit=cut_rounds, include_std=True)

print('-' * 50)
print(df_results)

print('-' * 50)
pd.options.display.float_format = '{:.4f}'.format
df_model_importance_norm = df_model_importance.div(df_model_importance.sum(axis=1), axis=0)
print(df_model_importance_norm)

print('-' * 50)
df_label_importance_norm = df_label_importance.div(df_label_importance.sum(axis=1), axis=0)
print(df_label_importance_norm)

print('-' * 50)

# fig, ax = plt.subplots(5, 4)
# ax = ax.ravel()
# for i, label in enumerate(all_labels):
#     ax[i].set_title(label)
#     for j, num_leaves in enumerate(num_leaves_opts):
#         ax[i].plot(all_histories[label][j]['train'], c='blue')
#         ax[i].plot(all_histories[label][j]['valid'], c='red')

# Optimize thresholds
best_score = 0
def thres_score(thresholds, tree_cut):
    global best_score
    result = f2_score(y_true, y_oof[tree_cut] > thresholds)
    print("{:6f}\r".format(result), end="")
    if result > best_score:
        best_score = result
        print()
    sys.stdout.flush()
    return result

best_thresholds = {}
default_thres = 0.2
n_steps = 20
n_iter = 4

for tree_cut_idx, tree_cut in enumerate(tree_cuts):
    print('-' * 30)
    best_score = 0
    print("Optimizing tree_cut {}".format(tree_cut))
    best_thresholds[tree_cut] = []
    eps = 1e-6

    if not isinstance(y_oof[tree_cut], np.ndarray):
        y_oof[tree_cut] = np.c_[y_oof[tree_cut]].T

    thersholds = [default_thres] * n_classes
    for idx, label in enumerate(all_labels):
        print("> {:10s}".format(label))
        
        x = np.linspace(y_oof[tree_cut][idx].min()+eps, y_oof[tree_cut][idx].max()-eps, n_steps)
        y = []
        for step in x:
            thersholds[idx] = step
            y.append(thres_score(thersholds, tree_cut))
            
        best_x = x[np.argmax(y)]
        thersholds[idx] = best_x
        best_thresholds[tree_cut].append(best_x)

    print("{:6}\r".format(best_score))

    # Theshold refine
    expand = 0.1

    for n in xrange(n_iter):
        print("\nBegin iter {}".format(n+1))
        new_best_thresholds = []

        thersholds = list(best_thresholds[tree_cut])
        for idx, label in enumerate(all_labels):
            print("> {:10s}".format(label))

            x = np.linspace(thersholds[idx]-expand, thersholds[idx]+expand, n_steps)
            y = []
            for step in x:
                thersholds[idx] = step
                y.append(thres_score(thersholds, tree_cut))

            best_y_idx = np.argmax(y)
            best_y = y[best_y_idx]
            
            # In case of a tie, find the mean index
            best_y_idx_last = best_y_idx
            while (y[best_y_idx_last] == best_y) and (best_y_idx_last < len(y)-1):
                best_y_idx_last += 1
            
            best_y_idx = (best_y_idx + best_y_idx_last) // 2
            
            best_x = x[best_y_idx]
            thersholds[idx] = best_x

            new_best_thresholds.append(best_x)

        score = thres_score(new_best_thresholds, tree_cut)

        best_thresholds[tree_cut] = new_best_thresholds

        expand *= 0.1
    
    best_score = 0

    print("Optimizing using minimization...")
    loss = lambda t: -thres_score(t, tree_cut)
    min_result = minimize(loss, best_thresholds[tree_cut], method='Nelder-Mead')
    min_result = minimize(loss, min_result.x, method='Powell')

    best_thresholds[tree_cut] = min_result.x

    thresholds = best_thresholds[tree_cut]

    print("\nExpected f2 after optimization: %.8f" % score)

    # Prepare submission
    y_pred_thres = df_sub_tree_cut[tree_cut][all_labels].values > thresholds

    df_sub_tree_cut[tree_cut]['tags'] = ''
    for label, yp in zip(all_labels, y_pred_thres.T):
        df_sub_tree_cut[tree_cut].loc[yp, 'tags'] = df_sub_tree_cut[tree_cut].loc[yp, 'tags'] + ' ' + label

    fname = 'sub_{:.5f}_cut_{:.2f}.csv.gz'.format(score, tree_cut)

    print("\nCreating file {}".format(fname))
    df_sub_tree_cut[tree_cut][['image_name', 'tags']].to_csv(fname, index=False, compression='gzip')
    df_sub_tree_cut[tree_cut].to_csv(fname.replace('sub_', 'sub_proba_'), index=False, compression='gzip')

    df_train_tree_cut = df_train.copy()
    for idx, label in enumerate(all_labels):
        df_train_tree_cut[label] = y_oof[tree_cut][:, idx]

    df_train_tree_cut.to_csv(fname.replace('sub_', 'sub_proba_train_'), index=False, compression='gzip')

    del df_train_tree_cut

