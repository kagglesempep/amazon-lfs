import os
import imp
import ctypes
import thread
import win32api

# Load the DLL manually to ensure its handler gets
# set before our handler.
ctypes.CDLL('C:\\Anaconda2\\Library\\bin\\libmmd.dll')
ctypes.CDLL('C:\\Anaconda2\\Library\\bin\\libifcoremd.dll')

# Now set our handler for CTRL_C_EVENT. Other control event 
# types will chain to the next handler.
def handler(dwCtrlType, hook_sigint=thread.interrupt_main):
    if dwCtrlType == 0: # CTRL_C_EVENT
        hook_sigint()
        return 1 # don't chain to the next handler
    return 0 # chain to the next handler

win32api.SetConsoleCtrlHandler(handler, 1)

print("Ctrl+C handler fixed")