from keras.layers import (Conv2D, MaxPooling2D, Input, Dropout, Dense, Flatten, Activation,
                         BatchNormalization, GlobalMaxPooling2D, GlobalAveragePooling2D, concatenate)
from keras.models import Sequential, Model
import keras.backend as K

def block_conv(x, name, n_convs, n_filters, filter_size, activation, bn):
    if K.image_data_format() == 'channels_last':
        bn_axis = 3
    else:
        bn_axis = 1

    for i in xrange(n_convs):
        x = Conv2D(n_filters, filter_size, padding='same', name='{}_conv{}'.format(name, i+1))(x)
        if bn:
            x = BatchNormalization(axis=bn_axis, name='{}_bn{}'.format(name, i+1))(x)
        x = Activation(activation)(x)

    x = MaxPooling2D((2, 2), name='{}_pool'.format(name))(x)

    return x

def cnn_simple(input_layer, n_blocks=5, n_convs_per_block=1, filter_size=3, activation='relu', base_filters=64, use_batchnorm=True):
    x = input_layer

    current_filters = base_filters

    for block in xrange(n_blocks):
        x = block_conv(x, 'block{}'.format(block+1), n_convs=n_convs_per_block, n_filters=current_filters,
            filter_size=filter_size, activation=activation, bn=use_batchnorm)
        current_filters *= 2

    return x


def cnn_simple_classify(input_shape, n_classes, last_activation='sigmoid', activation='relu', base_filters=64, dense_size=1024,
    dropout_p=0.2, use_batchnorm=True, global_pool=None):
    input_layer = Input(input_shape, name='input')

    # Create cnn layers
    x = cnn_simple(input_shape, 5, activation, base_filters, use_batchnorm)

    # Global pool block
    if global_pool is None:
        x = Flatten(name='flatten')(x)
    else:
        if not isinstance(global_pool, list):
            global_pool = [global_pool]
        
        out_pools = []
        for gp_name in global_pool:
            if gp_name == 'avg':
                pool = GlobalAveragePooling2D()(x)
            elif gp_name == 'max':
                pool = GlobalMaxPooling2D()(x)
            else:
                raise Exception("Invalid global pool: '{}'".format(gp_name))

            out_pools.append(pool)

        if len(out_pools) > 1:
            x = concatenate(out_pools)
        else:
            x = out_pools[0]

        # Classification block
        x = Dense(dense_size, activation=activation, name='fc1')(x)
        x = Dense(dense_size, activation=activation, name='fc2')(x)
        x = Dense(n_classes, activation=last_activation, name='predictions')(x)

    model = Model(input_layer, x, name='cnn_simple')

    return model