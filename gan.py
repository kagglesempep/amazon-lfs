from __future__ import print_function
import argparse
import os
import random
import torch
import torch.nn as nn
import torch.nn.init
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
from amazonds import Amazon, JoinDataset
import torchvision.transforms as transforms
import torchvision.utils as vutils
from torch.autograd import Variable
from collections import OrderedDict
import inspect
from savedmodel import SavedModel
import numpy as np
from progressbar import ProgressBar

skip_net = False

parser = argparse.ArgumentParser()
parser.add_argument('--dataset', default='train-weighted-noclass', help='all | train | test | train-weighted | train-weighted-noclass')
parser.add_argument('--datatype', default='jpg', help='jpg | tif')
parser.add_argument('--dataroot', default='../input', help='path to dataset')
parser.add_argument('--workers', type=int, help='number of data loading workers', default=2)
parser.add_argument('--batchSize', type=int, default=32, help='input batch size')
parser.add_argument('--imageSize', type=int, default=64, help='the height / width of the input image to network')
parser.add_argument('--nz', type=int, default=100, help='size of the latent z vector')
parser.add_argument('--ngf', type=int, default=64)
parser.add_argument('--ndf', type=int, default=64)
parser.add_argument('--niter', type=int, default=200, help='number of epochs to train for')
parser.add_argument('--lr', type=float, default=0.0002, help='learning rate, default=0.0002')
parser.add_argument('--std', type=float, default=0.02, help='normal std for weights initialization')
parser.add_argument('--beta1', type=float, default=0.6, help='beta1 for adam. default=0.5')
parser.add_argument('--cuda', action='store_true', default=True, help='enables cuda')
parser.add_argument('--netG', default='', help="path to netG (to continue training)")
parser.add_argument('--netD', default='', help="path to netD (to continue training)")
parser.add_argument('--modelroot', default=None, help='root model folder (ex: gan/model_0002)')
parser.add_argument('--outroot', default='gan', help='root folder to output images and model checkpoints')
parser.add_argument('--manualSeed', default=3523, help='manual seed')
parser.add_argument('--predict', action='store_true', default=False, help='predics netG on train and test')
parser.add_argument('--predpool', default='avg', help='avg or max pool used in predict mode')

opt = parser.parse_args()
print(opt)

if opt.predict and (opt.netD == ''):
    raise Exception('Specify netD when running in predict mode')

sm = SavedModel(opt.modelroot, root_folder=opt.outroot)

opt.modelroot = sm.folder

try:
    os.makedirs(opt.modelroot)
except OSError:
    pass

if opt.manualSeed is None:
    opt.manualSeed = random.randint(1, 10000)
print("Random Seed: ", opt.manualSeed)
random.seed(opt.manualSeed)
torch.manual_seed(opt.manualSeed)
if opt.cuda:
    torch.cuda.manual_seed_all(opt.manualSeed)

cudnn.benchmark = True

if torch.cuda.is_available() and not opt.cuda:
    print("WARNING: You have a CUDA device, so you should probably run with --cuda")

def RandomRotate(im):
    return im.rotate(random.randint(0, 4) * 90)

if opt.predict:
    transform = transforms.Compose([
        transforms.Scale(opt.imageSize),
        transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
    ])
else:
    transform = transforms.Compose([
        transforms.Scale(opt.imageSize),
        RandomRotate,
        transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
    ])

assert opt.dataset in {'all', 'train', 'test', 'train-weighted', 'train-weighted-noclass'}

amz_train = Amazon(opt.dataroot, opt.datatype, train=True, transform=transform)
amz_test = Amazon(opt.dataroot, opt.datatype, train=False, transform=transform)

if not opt.predict:
    if opt.dataset == 'all':
        dataset = JoinDataset([amz_train, amz_test], constant_target=0)
        sampler = None
        n_classes = 1  # only true or fake
    elif opt.dataset == 'train':
        dataset = amz_train
        sampler = None
        n_classes = 1 + 17  # true or fake plus all classes
    elif opt.dataset == 'test':
        dataset = amz_test
        sampler = None
        n_classes = 1  # only true or fake
    elif opt.dataset == 'train-weighted' or opt.dataset == 'train-weighted-noclass':
        # Try to balance the classes
        train_weight = (amz_train.y / amz_train.y.mean(axis=0)).max(axis=1)
        sampler = torch.utils.data.sampler.WeightedRandomSampler(train_weight.astype(float), train_weight.shape[0])
        dataset = amz_train

        if opt.dataset == 'train-weighted-noclass':
            n_classes = 1
        else:
            n_classes = 1 + 17  # true or fake plus all classes

    dataloader = torch.utils.data.DataLoader(dataset, batch_size=opt.batchSize, sampler=sampler,
                                            shuffle=True, num_workers=int(opt.workers))
else:
    n_classes = 1

nz = int(opt.nz)
ngf = int(opt.ngf)
ndf = int(opt.ndf)
nc = 3 if opt.datatype == 'jpg' else 4


# custom weights initialization called on netG and netD
def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, opt.std)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, opt.std)
        m.bias.data.fill_(0)

class View(nn.Module):
    def __init__(self, shape):
        super(View, self).__init__()
        self.shape = shape


    def forward(self, input):
        return input.view(*self.shape)


class _netG(nn.Module):
    def __init__(self, ngpu, imageSize):
        super(_netG, self).__init__()
        self.ngpu = ngpu

        filtersm = imageSize / 8

        main = nn.Sequential(OrderedDict([
            # input is Z, going into a convolution
            ('lin1', nn.Linear(nz+n_classes-1, nz)),
            ('relu_lin1', nn.ReLU(True)),
            ('lin2', nn.Linear(nz, nz)),
            ('relu_lin2', nn.ReLU(True)),
            ('view', View((-1, nz, 1, 1))),
            ('conv1', nn.ConvTranspose2d( nz, ngf *filtersm, 4, 1, 0, bias=False)),
            ('bn1', nn.BatchNorm2d(ngf *filtersm)),
            ('relu1', nn.ReLU(True))
        ]))

        fidx = 2
        while filtersm > 1: 
            main.add_module('conv%d'%fidx, nn.ConvTranspose2d(
                    ngf * filtersm, ngf * filtersm/2, 4, 2, 1, bias=False))

            main.add_module('bn%d'%fidx, nn.BatchNorm2d(ngf *filtersm/2))
            main.add_module('relu%d'%fidx, nn.ReLU(True))

            fidx += 1
            filtersm /= 2

        assert filtersm == 1
        main.add_module('conv%d'%fidx, nn.ConvTranspose2d(ngf, nc, 4, 2, 1, bias=False))
        main.add_module('tanh', nn.Tanh())

        self.main = main

    def forward(self, input):
        if isinstance(input.data, torch.cuda.FloatTensor) and self.ngpu > 1:
            output = nn.parallel.data_parallel(self.main, input, range(self.ngpu))
        else:
            output = self.main(input)
        return output


if not opt.predict:
    netG = _netG(1, opt.imageSize)
    netG.apply(weights_init)
    if opt.netG != '':
        netG.load_state_dict(torch.load(opt.netG))
    print(netG)


class _netD(nn.Module):
    def __init__(self, ngpu):
        super(_netD, self).__init__()
        self.ngpu = ngpu
        main = nn.Sequential(OrderedDict([
            # input is (nc) x 256 x 256
            ('conv1', nn.Conv2d(nc, ndf, 4, 2, 1, bias=False)),
            ('lrelu1', nn.LeakyReLU(0.2, inplace=True))
        ]))

        imsize = opt.imageSize / 2

        fidx = 2
        in_ndf = ndf
        while imsize > 4:
            main.add_module('conv%d'%fidx, nn.Conv2d(in_ndf, in_ndf * 2, 4, 2, 1, bias=False))
            main.add_module('bn%d'%fidx, nn.BatchNorm2d(in_ndf * 2))
            main.add_module('lrelu%d'%fidx, nn.LeakyReLU(0.2, inplace=True))

            in_ndf *= 2
            fidx += 1
            imsize /= 2

        main.add_module('conv%d'%fidx, nn.Conv2d(in_ndf, n_classes, 4, 1, 0, bias=False))
        main.add_module('sigmoid', nn.Sigmoid())

        self.main = main

    def forward(self, input):
        if isinstance(input.data, torch.cuda.FloatTensor) and self.ngpu > 1:
            output = nn.parallel.data_parallel(self.main, input, range(self.ngpu))
        else:
            output = self.main(input)

        return output.view(-1, 1)


netD = _netD(1)
netD.apply(weights_init)
if opt.netD != '':
    netD.load_state_dict(torch.load(opt.netD))
print(netD)


def var(clazz, shape):
    v = clazz(*shape)
    if opt.cuda:
        v = v.cuda()
    return Variable(v)


if opt.predict:
    if opt.cuda:
        netD.cuda()

    # Create feature net (netF):
    class _netF(nn.Module):
        def __init__(self, net):
            super(_netF, self).__init__()
            self.main = nn.Sequential(*list(net.children())[:-2])
            assert (opt.imageSize==128 and opt.ndf==128), "Implemented only for imageSize=128 and ndf=128"
            if opt.predpool == 'max':
                self.main.add_module('maxpool', nn.MaxPool2d(4))
            else:
                self.main.add_module('avgpool', nn.AvgPool2d(4))

        def forward(self, input):
            return self.main(input).squeeze()

    netF = _netF(netD.main)

    if opt.cuda:
        netF.cuda()

    input = var(torch.FloatTensor, (opt.batchSize, 3, opt.imageSize, opt.imageSize))

    for ds_name, ds in [('train', amz_train), ('test', amz_test)]:
        y_pred = []
        print("Predicting on {}...".format(ds_name))
        dataloader = torch.utils.data.DataLoader(ds, batch_size=opt.batchSize, shuffle=False, num_workers=int(opt.workers))
        bar = ProgressBar()
        for img, label in bar(dataloader):
            batch_size = img.size(0)

            input.data.resize_(img.size()).copy_(img)

            d_output = netF(input)

            y_pred.append(d_output.data.cpu())

        y_pred = torch.cat(y_pred, 0).numpy()

        np.save(os.path.join(opt.modelroot, 'y_{}_1pos_{}pool.npy'.format(ds_name, opt.predpool)), y_pred)


else:
    # Save _netD and _netG implementations
    def write_module_source(clz):
        cname = clz.__name__
        with open('%s/%s.py' % (opt.modelroot, cname), 'w') as f:
            f.write('class %s(nn.Module):\n' % cname)
            f.write(inspect.getsource(clz.__init__))
            f.write('\n')
            f.write(inspect.getsource(clz.forward))
        
    write_module_source(_netD)
    write_module_source(_netG)

    criterion = nn.BCELoss()

    input = torch.FloatTensor(opt.batchSize, 3, opt.imageSize, opt.imageSize)
    noise = torch.FloatTensor(opt.batchSize, nz)
    fixed_noise = torch.FloatTensor(opt.batchSize, nz).normal_(0, 1)
    if n_classes > 1:
        fixed_classes = torch.FloatTensor([[c == (r % (n_classes-1)) for c in xrange(n_classes-1)] for r in xrange(opt.batchSize)])
        fixed_noise = torch.cat([fixed_noise, fixed_classes], dim=1)

    label1dim = torch.FloatTensor(opt.batchSize, 1)
    real_label = 1
    fake_label = 0

    if opt.cuda:
        netD.cuda()
        netG.cuda()
        criterion.cuda()
        input, label1dim = input.cuda(), label1dim.cuda()
        noise, fixed_noise = noise.cuda(), fixed_noise.cuda()

    fixed_noise = Variable(fixed_noise)

    # setup optimizer
    optimizerD = optim.Adam(netD.parameters(), lr=opt.lr, betas=(opt.beta1, 0.999))
    optimizerG = optim.Adam(netG.parameters(), lr=opt.lr, betas=(opt.beta1, 0.999))

    if opt.dataset.startswith('train'):
        stats = np.zeros(17)
    else:
        stats = None

    step = -1

    for epoch in range(opt.niter):
        for i, data in enumerate(dataloader, 0):
            step = step + 1
            ############################
            # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
            ###########################
            # train with real
            netD.zero_grad()
            real_cpu, label_cpu = data

            if stats is not None:
                label_np = label_cpu.numpy()
                stats += np.bincount(np.where(label_np == 1)[1], minlength=17)

            batch_size = real_cpu.size(0)
            if opt.cuda:
                real_cpu = real_cpu.cuda()
                label_cpu = label_cpu.cuda()
            input.resize_as_(real_cpu).copy_(real_cpu)
            label1dim.resize_(batch_size, 1).fill_(real_label)
            inputv = Variable(input)
            labelv = Variable(torch.cat([label1dim, label_cpu], 1) if n_classes > 1 else label1dim)

            output = netD(inputv)
            errD_real = criterion(output, labelv)
            errD_real.backward()
            D_x = output.data.mean()

            # train with fake
            noise.resize_(batch_size, nz).normal_(0, 1)
            noisev = Variable(torch.cat([noise, label_cpu], 1) if n_classes > 1 else noise)
            fake = netG(noisev)
            label1dim.fill_(fake_label)
            labelv = Variable(torch.cat([label1dim, label_cpu], 1) if n_classes > 1 else label1dim)
            output = netD(fake.detach())
            errD_fake = criterion(output, labelv)
            errD_fake.backward()
            D_G_z1 = output.data.mean()
            errD = errD_real + errD_fake
            optimizerD.step()

            ############################
            # (2) Update G network: maximize log(D(G(z)))
            ###########################
            netG.zero_grad()
            label1dim.fill_(real_label)
            labelv = Variable(torch.cat([label1dim, label_cpu], 1) if n_classes > 1 else label1dim)
            output = netD(fake)
            errG = criterion(output, labelv)
            errG.backward()
            D_G_z2 = output.data.mean()
            optimizerG.step()

            print('[%d/%d][%d/%d] Loss_D: %.4f Loss_G: %.4f D(x): %.4f D(G(z)): %.4f / %.4f'
                % (epoch, opt.niter, i, len(dataloader),
                    errD.data[0], errG.data[0], D_x, D_G_z1, D_G_z2))

            if i % 100 == 0:
                if i == 0 and epoch == 0:
                    vutils.save_image(real_cpu,
                            '%s/real_samples.png' % opt.modelroot,
                            normalize=True)
                fake = netG(fixed_noise)
                vutils.save_image(fake.data,
                        '%s/fake_samples_epoch_%03d.png' % (opt.modelroot, epoch),
                        normalize=True)

        if stats is not None:
            print('-' * 50)
            print("Stats:")
            for stat, col in zip(stats, amz_train.df.columns[1:]):
                print("{:20s}: {:.4f}".format(col, stat / (step * batch_size)))

        # do checkpointing
        torch.save(netG.state_dict(), '%s/netG_epoch_%d.pth' % (opt.modelroot, epoch))
        torch.save(netD.state_dict(), '%s/netD_epoch_%d.pth' % (opt.modelroot, epoch))
