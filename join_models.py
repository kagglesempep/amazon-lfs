from __future__ import print_function
import pandas as pd
import numpy as np
from os.path import join, exists
import glob
import sys
from scipy.optimize import minimize
from sklearn.metrics import fbeta_score

if len(sys.argv) < 3:
    print("Please specify models folders as parameter")
    sys.exit(1)

models = sys.argv[1:]

for m in models:
    if not exists(m):
        print("Folder '{}' does not exists".format(m))
        sys.exit(2)

all_labels = ['agriculture', 'artisinal_mine', 'bare_ground', 'blooming',
       'blow_down', 'clear', 'cloudy', 'conventional_mine', 'cultivation',
       'habitation', 'haze', 'partly_cloudy', 'primary', 'road',
       'selective_logging', 'slash_burn', 'water']

get_model_csv = lambda f: glob.glob(join(f, 'model_*.csv.gz'))[0]
get_y_oof = lambda f: join(f, 'y_oof_unsorted.npz')

y_true = []
y_pred = []
y_sub = []

for m in models:
    y_oof = np.load(get_y_oof(m))

    y_true.append(y_oof['y_true'])
    y_pred.append(y_oof['y_pred'])

    y_sub.append(pd.read_csv(get_model_csv(m), usecols=all_labels).values)

y_true = np.mean(y_true, axis=0)
y_pred = np.mean(y_pred, axis=0)
y_sub = np.mean(y_sub, axis=0)

def f2_score(y_true, y_pred):
    y_true, y_pred, = np.array(y_true), np.array(y_pred)
    return fbeta_score(y_true, y_pred, beta=2, average='samples')

# Optimize thresholds
def thres_loss(thresholds):
     result = -f2_score(y_true, y_pred > thresholds)
     print(result, thresholds)
     return result

initial_thresholds = np.array([0.5] * len(all_labels))

# Optimize process
min_result = minimize(thres_loss, initial_thresholds, method='Powell')

print("Expected f2 after optimization: %.4f" % min_result.fun)

df_sub = pd.read_csv(join('..', 'input', 'sample_submission_v2.csv'))

# Prepare submission
y_pred_thres = y_sub > min_result.x

df_sub['tags'] = ''
for label, yp in zip(all_labels, y_pred_thres.T):
    df_sub.loc[yp, 'tags'] = df_sub.loc[yp, 'tags'] + ' ' + label

df_sub[['image_name', 'tags']].to_csv('sub_%.4f.csv' % -min_result.fun, index=False)
