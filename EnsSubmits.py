# -*- coding: utf-8 -*-

import csv
from glob import glob

glob_files = '../submit/*.csv'

weightbase = 1.
wfactor = 1.
Hlabel = 'image_name'
Htarget = 'tags'

## Weights of the individual subs ##
lg = len(glob(glob_files))
subweight = [0]*lg
sub = [None]*lg

for i, glob_file in enumerate( glob(glob_files) ):
    subweight[i] = float(glob_file.split('_')[1].replace('.csv',''))
    ## input files ##
    print("Reading {}: w={} - {}". format(i, subweight[i], glob_file))
    reader = csv.DictReader(open(glob_file,"r"))
    sub[i] = sorted(reader, key=lambda d: d[Hlabel])
    weightbase = round(wfactor*weightbase,2)

thr = 0.9*sum(subweight)/lg

## output file ##
out = open("subens{}_{}_.csv".format(lg,thr), "w", newline='')
writer = csv.writer(out)
writer.writerow([Hlabel,Htarget])
p=0
for row in sub[0]:
    target_weight = {}
    for ind, trgt in enumerate(row[Htarget].split()):
        target_weight[trgt] = target_weight.get(trgt,0)
    for s in range(lg-1):
        row1 = sub[s+1][p]
        for ind, trgt in enumerate(row1[Htarget].split()):
            target_weight[trgt] = target_weight.get(trgt,0) + subweight[0]/sum(subweight)
    target_weight = { k:v for k, v in target_weight.items() if v>=thr }
    tops_trgt = sorted(target_weight, key=target_weight.get, reverse=True)
    writer.writerow([row1[Hlabel], " ".join(tops_trgt)])
    p+=1
out.close()
