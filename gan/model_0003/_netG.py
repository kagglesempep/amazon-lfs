class _netG(nn.Module):
    def __init__(self, ngpu):
        super(_netG, self).__init__()
        self.ngpu = ngpu

        filtersm = opt.imageSize / 8

        main = nn.Sequential(OrderedDict([
            # input is Z, going into a convolution
            ('conv1', nn.ConvTranspose2d( nz, ngf *filtersm, 4, 1, 0, bias=False)),
            ('bn1', nn.BatchNorm2d(ngf *filtersm)),
            ('relu1', nn.ReLU(True))
        ]))

        fidx = 2
        while filtersm > 1:
            main.add_module('conv%d'%fidx, nn.ConvTranspose2d(
                    ngf * filtersm, ngf * filtersm/2, 4, 2, 1, bias=False))

            main.add_module('bn%d'%fidx, nn.BatchNorm2d(ngf *filtersm/2))
            main.add_module('relu%d'%fidx, nn.ReLU(True))

            fidx += 1
            filtersm /= 2

        assert filtersm == 1
        main.add_module('conv%d'%fidx, nn.ConvTranspose2d(ngf, nc, 4, 2, 1, bias=False))
        main.add_module('tanh', nn.Tanh())

        self.main = main

    def forward(self, input):
        if isinstance(input.data, torch.cuda.FloatTensor) and self.ngpu > 1:
            output = nn.parallel.data_parallel(self.main, input, range(self.ngpu))
        else:
            output = self.main(input)
        return output
