class _netD(nn.Module):
    def __init__(self, ngpu):
        super(_netD, self).__init__()
        self.ngpu = ngpu
        main = nn.Sequential(OrderedDict([
            # input is (nc) x 256 x 256
            ('conv1', nn.Conv2d(nc, ndf, 4, 2, 1, bias=False)),
            ('lrelu1', nn.LeakyReLU(0.2, inplace=True))
        ]))

        imsize = opt.imageSize / 2

        fidx = 2
        in_ndf = ndf
        while imsize > 4:
            main.add_module('conv%d'%fidx, nn.Conv2d(in_ndf, in_ndf * 2, 4, 2, 1, bias=False))
            main.add_module('bn%d'%fidx, nn.BatchNorm2d(in_ndf * 2))
            main.add_module('lrelu%d'%fidx, nn.LeakyReLU(0.2, inplace=True))

            in_ndf *= 2
            fidx += 1
            imsize /= 2

        main.add_module('conv%d'%fidx, nn.Conv2d(in_ndf, 1, 4, 1, 0, bias=False))
        main.add_module('sigmoid', nn.Sigmoid())

        self.main = main

    def forward(self, input):
        if isinstance(input.data, torch.cuda.FloatTensor) and self.ngpu > 1:
            output = nn.parallel.data_parallel(self.main, input, range(self.ngpu))
        else:
            output = self.main(input)

        return output.view(-1, 1)
