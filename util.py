from __future__ import  print_function
import re
import glob
import os
import weakref
import numpy as np
from collections import Counter, deque
from sklearn.metrics import fbeta_score
import time
from sklearn.metrics import roc_auc_score
from keras.callbacks import Callback, LearningRateScheduler
import keras.backend as K
from keras.models import Model
import threading
try:
    import lightgbm as lgb
except:
    pass

class LRUCacheDynamicSize:
    def __init__(self, factory, num_new_items_available, eval_every=10000, verbose=True):
        assert callable(num_new_items_available), "'num_new_items_available' must be callable"
        self.factory = factory
        self.eval_every = eval_every
        self.num_new_items_available = num_new_items_available
        self.verbose = verbose
        self.num_fetch = 0
        self.weak = weakref.WeakValueDictionary()
        self.strong = deque()
        self.lock = threading.Lock()
        self.notFound = object()
        self.maxlen = 0 # We will evaluate thie value on first call

    def __call__(self, key):
        if self.num_fetch % self.eval_every == 0:
            new_size = max(0, self.num_new_items_available() + len(self.strong))
            if self.verbose:
                print('{} cache from {} to {} entries'.format('Raising' if new_size > self.maxlen else 'Lowering', self.maxlen, new_size))
            self.maxlen = new_size
    
            with self.lock:
                while len(self.strong) > self.maxlen:
                    self.strong.popleft()

        self.num_fetch += 1
        
        value = self.weak.get(key, self.notFound)
        if value is self.notFound:
            self.weak[key] = value = self.factory(key)


        with self.lock:
            self.strong.append(value)

        return value

class XGBCVHolder:
    """
    This is a hack to XGBoost, which does not provide an API to access 
    the models trained over xgb.cv
    """
    def __init__(self, features_names, dtrain, folds):
        self.models = []
        self.dtrain = dtrain
        self.dtests = []
        self.called = False
        self.folds = folds
        self.features_names = features_names

    @staticmethod
    def from_holder(other):
        new_holder = XGBCVHolder(other.features_names, other.dtrain, other.folds)
        new_holder.models = other.models
        new_holder.dtests = other.dtests

        return new_holder

    @staticmethod
    def from_cv(features_names, dtrain, folds, models, dtests):
        new_holder = XGBCVHolder(features_names, dtrain, folds)
        new_holder.models = models
        new_holder.dtests = dtests

        return new_holder

    def __call__(self, env):
        if not self.called:
            self.called = True

            # XGB holder
            if hasattr(env, 'cvfolds'):
                for cvpack in env.cvfolds:
                    self.models.append(cvpack.bst)
                    self.dtests.append(cvpack.dtest)

            # LightGBM holder
            else:
                raise Exception("This implementation has a data leak... must use dtrain.reference")
                self.models = env.model.boosters
                for m, (train_idx, test_idx) in zip(self.models, self.folds):
                    dtest = lgb.Dataset(self.dtrain.data[test_idx], self.dtrain.label[test_idx], feature_name=self.features_names)
                    self.dtests.append(dtest)


    def predict_oof(self, ntree_limit=0):
        y = self.dtrain.get_label()
        y_hat = np.zeros_like(y, dtype=np.float)
        for model, dtest, (idx_train, idx_test) in zip(self.models, self.dtests, self.folds):
            y_test = dtest.get_label()
            assert np.all(y[idx_test] == y_test), "Labels mismatch"

            # LightGBM
            if isinstance(model, lgb.Booster):
                y_hat[idx_test] = model.predict(dtest.reference.data[dtest.used_indices], num_iteration=ntree_limit)

            # XGB
            else:
                y_hat[idx_test] = model.predict(dtest, ntree_limit=ntree_limit)

        return y, y_hat

    def predict_mean(self, xtrain, ntree_limit=0, include_std=False):
        y_hat = []
        for m in self.models:
            m.feature_names = None # Hack to accept any columns names, because xgb.cv drops feature_names from folds data

            # LightGBM
            if isinstance(m, lgb.Booster):
                y_hat.append(m.predict(xtrain.data, num_iteration=ntree_limit))

            # XGB
            else:
                y_hat.append(m.predict(xtrain, ntree_limit=ntree_limit))

        if include_std:
            return np.mean(y_hat, axis=0), np.std(y_hat, axis=0)
        else:
            return np.mean(y_hat, axis=0)

    def get_fscore(self):
        total = Counter()

        for m in self.models:
            # LightGBM
            if isinstance(m, lgb.Booster):
                fscore = m.feature_importance("gain")
                fixed_features_names = { self.features_names[k] : v for k, v in enumerate(fscore) }

            # XGB
            else:
                fscore = m.get_fscore()
                fixed_features_names = { self.features_names[int(k[1:])] : v for k, v in fscore.iteritems() }
            total.update(fixed_features_names)

        return total

# From here: https://www.kaggle.com/anokas/planet-understanding-the-amazon-from-space/fixed-f2-score-in-python/code
def f2_score(y_true, y_pred):
    y_true, y_pred, = np.array(y_true), np.array(y_pred)
    return fbeta_score(y_true, y_pred, beta=2, average='samples')


# from here https://gist.github.com/smly/d29d079100f8d81b905e
class IntervalEvaluation(Callback):
    def __init__(self, validation_data=()):
        super(Callback, self).__init__()
        self.X_val, self.y_val = validation_data

    def on_epoch_end(self, epoch, logs={}):
        start = time.time()
        y_pred = self.model.predict(self.X_val, verbose=0)
        score = roc_auc_score(self.y_val, y_pred)
        end = time.time()
        print(" val_roc: {:.6f} (calc in {:.2f}s)".format(score, end-start))

        logs['val_roc'] = score

        # It shouldn't be here... but it is too conveinent to removed :)
        logs['lr'] = K.get_value(self.model.optimizer.lr)

def lr_scheduler(start_lr, top_lr, stop_lr, warnup_epochs, stop_epoch):
    def scheduler(epoch):
        # Warn-up stage
        if epoch <= warnup_epochs:
            lr = start_lr + epoch * (top_lr-start_lr) / warnup_epochs

        # After stop epoch
        elif epoch >= stop_epoch:
            lr = stop_lr

        # Between top and stop
        else:
            lr = top_lr - (epoch-warnup_epochs) * (top_lr - stop_lr) / (stop_epoch-warnup_epochs)

        return lr

    return scheduler

class WarnupLearningRateScheduler(LearningRateScheduler):
    def __init__(self, start_lr, top_lr, stop_lr, warnup_epochs, stop_epoch):
        callback = lr_scheduler(start_lr, top_lr, stop_lr, warnup_epochs, stop_epoch)
        super(WarnupLearningRateScheduler, self).__init__(callback)

    def on_epoch_end(self, epoch, logs=None):
        super(WarnupLearningRateScheduler, self).on_epoch_end(epoch, logs)
        
        logs = logs or {}
        logs['lr'] = K.get_value(self.model.optimizer.lr)

# From http://anandology.com/blog/using-iterators-and-generators/
class threadsafe_iter:
    """Takes an iterator/generator and makes it thread-safe by
    serializing call to the `next` method of given iterator/generator.
    """
    def __init__(self, it):
        self.it = it
        self.lock = threading.Lock()

    def __iter__(self):
        return self

    def next(self):
        with self.lock:
            return self.it.next()

def threadsafe_generator(f):
    """A decorator that takes a generator function and makes it thread-safe.
    """
    def g(*a, **kw):
        return threadsafe_iter(f(*a, **kw))
    return g


f2_epsilon = 1e-4
f2_smooth = 1e-4


def f2_loss(y_true, y_pred):
    beta = 2
    beta2 = beta ** 2

    y_pred = K.clip(y_pred, f2_epsilon, 1.0 - f2_epsilon)
    y_true = K.clip(y_true, f2_epsilon, 1.0 - f2_epsilon)

    true_and_pred = y_true * y_pred
    ttp_sum = K.sum(true_and_pred, axis=1)
    tpred_sum = K.sum(y_pred, axis=1)
    ttrue_sum = K.sum(y_true, axis=1)

    tprecision = ttp_sum / tpred_sum
    trecall = ttp_sum / ttrue_sum

    tf_score = ((1 + beta2) * tprecision * trecall + f2_smooth) / (beta2 * tprecision + trecall + f2_smooth)

    return -K.mean(tf_score)


# From here: https://github.com/Hakuyume/chainercv/blob/master/chainercv/transforms/image/pca_lighting.py
def pca_lighting(img, sigma=0.2, eigen_value=None, eigen_vector=None):
    """AlexNet style color augmentation
    This method adds a noise vector drawn from a Gaussian. The direction of
    the Gaussian is same as that of the principal components of the dataset.
    This method is used in training of AlexNet [1].
    .. [1] Alex Krizhevsky, Ilya Sutskever, Geoffrey E. Hinton. \
    ImageNet Classification with Deep Convolutional Neural Networks. \
    NIPS 2012.
    Args:
        image (numpy.ndarray): An image array to be augmented. This is in
            CHW format.
        sigma (float): Standard deviation of the Gaussian. In the original
            paper, this value is 10% of the range of intensity
            (25.5 if the range is [0, 255]).
        eigen_value: (numpy.ndarray): An array of eigen values. The shape
            have to be (3,). If it is not specified, the values computed from
            ImageNet are used.
        eigen_vector: (numpy.ndarray): An array of eigen vectors. The shape
            have to be (3, 3). If it is not specified, the vectors computed
            from ImageNet are used.
    Returns:
        An image in CHW format.
    """

    if sigma <= 0:
        return img

    # these values are copied from "PCA Colors" notebook
    if eigen_value is None:
        eigen_value = np.array([ 2.59639858,  0.05281296,  0.00470951])
    if eigen_vector is None:
        eigen_vector = np.array(
            [[ 0.56647001,  0.58186674,  0.58356047],
             [ 0.6882782 ,  0.05537043, -0.72333065],
             [ 0.45319405, -0.81139707,  0.3691205 ]])

    alpha = np.random.normal(0, sigma, size=3)

    img = img.copy()
    img += eigen_vector.dot(eigen_value * alpha)

    return img


def toggle_model_trainable(model, compile=True):
    for layer in model.layers:
        if isinstance(layer, Model):
            toggle_model_trainable(layer, compile=False)

        layer.trainable = not layer.trainable

    if compile:
        model.compile(model.optimizer, model.loss, model.metrics)


def set_model_trainable(model, first_trainable_layer, trainable, compile=True):
    found_layer = False
    for layer in model.layers:
        if layer.name == first_trainable_layer:
            found_layer = True
            break
        elif isinstance(layer, Model):
            sub_result = set_model_trainable(layer, first_trainable_layer, trainable, compile=False)
            if sub_result:
                found_layer = True
                break
            else:
                layer.trainable = trainable
        else:
            layer.trainable = trainable

    if compile:
        model.compile(model.optimizer, model.loss, model.metrics)

    return found_layer

