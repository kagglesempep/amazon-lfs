from __future__ import print_function
import numpy as np
import lightgbm as lgb
from util import XGBCVHolder
import time

def cv(params, X, y, folds, num_boost_round=2000, early_stopping_rounds=30, verbose_eval=20,
       maximize=False, features_names=None):
    n_folds = len(folds)

    dtrain_all = lgb.Dataset(X, y, feature_name=features_names, free_raw_data=False)

    cvdata = []
    boosters = []
    dvalids = []
    for train, val in folds:
        dtrain = dtrain_all.subset(train)
        dval = dtrain_all.subset(val)

        dvalids.append(dval)

        cvdata.append((dtrain, dval))
    
        booster = lgb.Booster(params, dtrain)
        booster.add_valid(dval, 'valid')

        boosters.append(booster)

    best_score = -np.inf if maximize else np.inf
    best_round = 0
    results = {'train':[], 'valid':[]}
    last_eval = None
    for n_round in xrange(num_boost_round):
        round_train_scores = []
        round_valid_scores = []
        for booster in boosters:
            booster.update()

            round_train_scores.append(booster.eval_train()[0][2])
            round_valid_scores.append(booster.eval_valid()[0][2])

        train_score = np.mean(round_train_scores)
        valid_score = np.mean(round_valid_scores)

        results['train'].append(train_score)
        results['valid'].append(valid_score)

        if n_round % verbose_eval == 0:
            now = time.time()

            if last_eval:
                str_time = " (in {:3.2f}s)".format(now-last_eval)
            else:
                str_time = ""

            print("[{:04d}] train: {:7.5f} valid: {:7.5f}{}".format(n_round, train_score, valid_score, str_time))
            last_eval = now


        if maximize:
            if valid_score > best_score:
                best_score = valid_score
                best_round = n_round
        else:
            if valid_score < best_score:
                best_score = valid_score
                best_round = n_round
            
        if n_round - best_round > early_stopping_rounds:
            break

    xgb_holder = XGBCVHolder.from_cv(features_names, dtrain_all, folds, boosters, dvalids)

    return best_score, best_round, xgb_holder, results