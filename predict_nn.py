import collections
import glob
import weakref
from multiprocessing import Pool
from os.path import exists, join, split

import keras.backend as K
import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
import psutil
from keras.layers import Input
from keras.models import Model, load_model
#from keras.applications.imagenet_utils import preprocess_input
from keras.applications.inception_v3 import preprocess_input
from progressbar import ProgressBar
from scipy.optimize import minimize
from skimage.io import imread
from skimage.transform import downscale_local_mean
from sklearn import metrics
from sklearn.model_selection import KFold
import re
import cv2
from util import f2_score, f2_loss
from savedmodel import SavedModel
from scale_layer import Scale
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--usetest', action='store_true', help='predict on test')
parser.add_argument('--model_folder', required=True, default='models/model_0021', help='model folder')

opt = parser.parse_args()
print(opt)

train_classes = ['agriculture', 'artisinal_mine', 'bare_ground', 'blooming',
       'blow_down', 'clear', 'cloudy', 'conventional_mine', 'cultivation',
       'habitation', 'haze', 'partly_cloudy', 'primary', 'road',
       'selective_logging', 'slash_burn', 'water']

n_classes = len(train_classes) if train_classes else 17

batch_size = 16
n_folds = 4
image_float = 'float32'
image_type = 'jpg' # one of 'jpg', 'tif' or 'tif3'
imagenet_norm = True
std_multi = 1
split_test = None #[False, True]
truncate_layer = None #'vgg19'
gen_8pos = False

width, height, n_channels = 299, 299, (4 if image_type == 'tif' else 3)

im_shape = (width, height, n_channels) if K.image_data_format() == 'channels_last' else (n_channels, width, height)
dim_axes = (0, 1) if K.image_data_format() == 'channels_last' else (1, 2)
channel_axis = -1 if K.image_data_format() == 'channels_last' else 0

if opt.usetest:
    imgs_folder = '../input/test-jpg' if image_type == 'jpg' else '../input/test-tif-v2' # SSD folder
else:
    imgs_folder = '../input/train-jpg' if image_type == 'jpg' else '../input/train-tif-v2' # SSD folder

# Leave 2GB free
num_cached_images = 64  # We will not reuse a lot of images


# See Data Exploration.ipynb for details
if image_type == 'jpg':
    means_per_channel = np.array([ 77.95814168, 85.57786945, 75.08671441])
    stds_per_channel  = np.array([ 41.65053961, 35.81152416, 34.00985153])
elif image_type == 'tif3':
    means_per_channel = np.array([ 2.67706894e-01, 4.23189691e+03, 6.30784523e-01 ]) / std_multi
    stds_per_channel  = np.array([ 1.50306188e+00,  1.53323986e+03, 2.67391302e+00 ]) / std_multi
else:
    means_per_channel = np.array([ 4947.8677349 , 4231.89690504, 3031.3910505 , 6397.284506  ]) / std_multi
    stds_per_channel  = np.array([ 1682.94966902, 1533.23985562, 1575.38137029, 1840.84254538]) / std_multi

# Load metadata
def load_img(f):
    im = imread(join(imgs_folder, f + '.' + image_type[:3])).astype(np.float32)

    if image_type == 'tif3':
        b, g, r, nir = im[...,0], im[...,1], im[...,2], im[...,3]
        nvdi = (nir-r) / (nir+r)
        gamma = 1
        rb = r - gamma * (b-r)
        arvi = (nir - rb) / (nir + rb)
        im = np.stack([arvi, g, nvdi], axis=2)

    # Simplify process of normalization for jpg/imagenet
    if (image_type == 'jpg') and imagenet_norm:
        im = preprocess_input(im[None])[0]
    else:
        im -= means_per_channel
        im /= stds_per_channel

        if imagenet_norm:
            if n_channels == 4:
                im *= [61.11662676,  60.33262408,  65.21923796, 60.]
                im += [ 124.9789042 ,  122.63242437,  113.3726225, 120. ]
            else:
                im *= [61.11662676,  60.33262408,  65.21923796]
                im += [ 124.9789042 ,  122.63242437,  113.3726225 ]


    if (width != 256) or (height != 256):
        im = cv2.resize(im, (width, height), interpolation=cv2.INTER_AREA)

    if K.image_data_format() == 'channels_first':
        im = np.rollaxis(im, 2)

    return im.astype(image_float)


if opt.usetest:
    df_all = pd.read_csv('../input/sample_submission_v2.csv')
else:
    df_all = pd.read_csv('../input/train_v2.csv')
    df_all = pd.concat([df_all['image_name'], df_all.tags.str.get_dummies(sep=' ')], axis=1)


cached_load_img = load_img


def gen_xy(df, y=None, batch_size=32, progress=False):
    global cached_load_img

    num = df.shape[0]

    num_batches = num // batch_size
    if num % batch_size != 0:
        num_batches += 1

    def local_gen():
        while True:
            i = 0
            X_batch = []

            if progress:
                bar = ProgressBar(max_value=df.shape[0])

            while i < df.shape[0]:
                bar.update(i)
                im_name = df.iloc[i]['image_name']
                im = load_img(im_name)

                X_batch.append(im)

                if len(X_batch) == batch_size:
                    yield np.array(X_batch)
                    X_batch = []

                i = i+1

            if len(X_batch) > 0:
                yield np.array(X_batch)
        
    return local_gen(), num_batches

def gen_xy_8pos(df, y=None, batch_size=32, progress=False):
    global cached_load_img

    assert batch_size % 8 == 0, "Batch size must be a multiple of 8"

    num = df.shape[0] * 8

    num_batches = num // batch_size
    if num % batch_size != 0:
        num_batches += 1

    def local_gen():
        while True:
            i = 0
            X_batch = []

            if progress:
                bar = ProgressBar(max_value=df.shape[0])

            while i < df.shape[0]:
                bar.update(i)
                im_name = df.iloc[i]['image_name']
                im = load_img(im_name)

                for j in xrange(8):
                    rotate = j // 2
                    flip = j % 2

                    # Apply rotation and flip
                    imj = np.rot90(im, rotate, axes=dim_axes)
                    if flip:
                        imj = np.flip(imj, axis=dim_axes[0])
                    
                    X_batch.append(imj)

                if len(X_batch) == batch_size:
                    yield np.array(X_batch)
                    X_batch = []

                i = i+1

            if len(X_batch) > 0:
                yield np.array(X_batch)
        
    return local_gen(), num_batches


models_files = glob.glob(join(opt.model_folder, "*.model"))

# Check for label-specific models files...
found_labels = set()
for m in models_files:
    found_label = re.findall(r'nn_([\w_]+)_fold_\d.model', split(m)[1])
    found_labels |= set(found_label)
    

if len(found_labels) == 0:
    found_labels = [None]
else:
    found_labels = sorted(found_labels)

print("Found Labels:")
print(found_labels)

def f2_bin_loss(y_true, y_pred):
    return -1 / f2_loss(y_true, y_pred)

def f2_score(y_true, y_pred):
    return -f2_loss(y_true, y_pred)

def load_model_f2(f):
    model = load_model(f, custom_objects={'f2_loss': f2_loss, 'f2_score': f2_score, 'Scale': Scale, 'f2_bin_loss': f2_bin_loss})

    if truncate_layer:
        model = Model(inputs=model.input, outputs=model.get_layer(truncate_layer).get_output_at(1))

    print(model.summary())

    return model

str_npos = '8pos' if gen_8pos else '1pos'

if opt.usetest:
    for label in found_labels:
        if label is not None:
            print("Evaluating folds of label {}...".format(label))
            models_files_label = sorted(glob.glob(join(opt.model_folder, "nn_{}_fold_*.model".format(label))))
        else:
            models_files_label = models_files

        print("Loading models...")
        models = map(load_model_f2, models_files_label)

        for i, m in enumerate(models):
            print("model {}: in={} out={}".format(i, str(m.layers[0].get_input_shape_at(0)), str(m.layers[-1].get_output_shape_at(0))))
            m.name = 'model_{}'.format(i+1)

        n_labels = models[0].get_output_shape_at(0)[1]
            
        input_layer = Input(im_shape, name='input')

        models_out = list(map(lambda m: m(input_layer), models))

        model = Model(input_layer, models_out)

        if split_test is not None:
            n_folds_test = len(split_test)
            test_fold = np.where(split_test)[0][0]
            print("> Training on split {}".format(test_fold))
            fold_index = list(KFold(n_folds_test, random_state=42).split(df_all))[test_fold][1]

            df_all = df_all.iloc[fold_index]

            y_test_fname = 'y_test_{}_split_{}.npy'.format(str_npos, test_fold) if label is None else 'y_test_{}_{}_split_{}.npy'.format(str_npos, label, test_fold)
        else:
            y_test_fname = 'y_test_{}.npy'.format(str_npos) if label is None else 'y_test_{}_{}.npy'.format(str_npos, label)

        if truncate_layer:
            y_test_fname = y_test_fname.replace('.npy', '_trunc_{}.npy'.format(truncate_layer))

        print("> Generating file {}...".format(y_test_fname))

        generator = gen_xy_8pos if gen_8pos else gen_xy

        gen_test, num_test = generator(df_all, batch_size=batch_size, progress=True)

        # This prediction will take a lot of time...
        y_test_8pos = model.predict_generator(gen_test, num_test)

        # Save y_8pos
        y_test_8pos = np.array(y_test_8pos).reshape(-1, df_all.shape[0], 8 if gen_8pos else 1, n_labels) # Note: this operation is sentive to byte-order of original array


        np.save(join(opt.model_folder, y_test_fname), y_test_8pos)

        del model
        K.clear_session()

else:
    for label in found_labels:
        if label is not None:
            print("Evaluating folds of label {}...".format(label))
            kf = KFold(n_folds, random_state=42).split(df_all, df_all[label])
        else:
            kf = KFold(n_folds, random_state=42).split(df_all)

        y_oof_8pos = None

        for fold_num, (train_index, val_index) in enumerate(kf):
            print("Evaluating fold %d...\n"% fold_num)
            model_base_file = "nn_fold_{}.model".format(fold_num) if label is None else "nn_{}_fold_{}.model".format(label, fold_num)

            model_file = join(opt.model_folder, model_base_file)
            model = load_model_f2(model_file)

            n_labels = model.get_output_shape_at(0)[1]

            if y_oof_8pos is None:
                y_oof_8pos = np.empty((df_all.shape[0], 8 if gen_8pos else 1, n_labels))

            df_val = df_all.iloc[val_index]

            generator = gen_xy_8pos if gen_8pos else gen_xy

            gen_val, num_val = generator(df_val, batch_size=batch_size, progress=True)

            y_val_8pos = model.predict_generator(gen_val, num_val)

            y_oof_8pos[val_index] = np.array(y_val_8pos).reshape(len(val_index), 8 if gen_8pos else 1, n_labels)

            del model
            K.clear_session()

        y_oof_fname = 'y_oof_{}.npy'.format(str_npos) if label is None else 'y_oof_{}_{}.npy'.format(str_npos, label)

        if truncate_layer:
            y_oof_fname = y_oof_fname.replace('.npy', '_trunc_{}.npy'.format(truncate_layer))

        np.save(join(opt.model_folder, y_oof_fname), y_oof_8pos)
