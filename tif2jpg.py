 __future__ import print_function
import numpy as np
from skimage.io import imread
from skimage.transform import downscale_local_mean
from os.path import join, split, splitext
import glob
from scipy.spatial import distance
from scipy.optimize import minimize, curve_fit
import matplotlib.pyplot as plt

num_samples = 800
downscale_factor = 8
im_side = 256
function_resoution = 32
percentiles_x = [1, 99]

folder_tif = join('..', 'input', 'train-tif')
folder_jpg = join('..', 'input', 'train-jpg')

# Util lambdas
filename = lambda f: splitext(split(f)[-1])[0]
downscale_im = lambda im: downscale_local_mean(im, (downscale_factor, downscale_factor, 1))
to_uint8 = lambda im: (im+0.5).clip(0, 255).astype(np.uint8)

# Find tif files to use
tif_files = glob.glob(join(folder_tif, "*.tif"))

# Choose files to use
tif_files = [tif_files[i] for i in np.random.choice(range(len(tif_files)), num_samples, replace=False)]
jpg_files = map(lambda f: join(folder_jpg, filename(f) + '.jpg'), tif_files)

tif_data = np.array(map(downscale_im, map(imread, tif_files)))
jpg_data = np.array(map(downscale_im, map(imread, jpg_files)))

assert tif_data.shape == (num_samples, im_side // downscale_factor, im_side // downscale_factor, 4)
assert jpg_data.shape == (num_samples, im_side // downscale_factor, im_side // downscale_factor, 3)

# As we are looking for three functions to map R, G and B channels from TIF to JPG, we can discard the NIR channel of
# TIF images
tif_data = tif_data[..., :3]

funcs_per_channel = []

# Find a trivial transformation from x to y, using matched percentiles
x_per_channel = [np.percentile(tif_data[...,c].ravel(), np.linspace(0, 100, function_resoution)) for c in xrange(3)]
y_per_channel = [np.percentile(jpg_data[...,c].ravel(), np.linspace(0, 100, function_resoution)) for c in xrange(3)]

plot_results = False

for channel in xrange(3):
    x = x_per_channel[channel]
    y = y_per_channel[channel]

    jpg_channel_pixels = jpg_data[...,2-channel]  # Channels are R, G and B
    tif_channel_pixels = tif_data[...,channel]    # Channels are B, G and R

    y_singles = []

    bar = ProgressBar()
    for jpg_im, tif_im in bar(zip(jpg_channel_pixels, tif_channel_pixels)):
        loss = lambda y_candidate: \
              distance.euclidean(np.interp(tif_im.ravel(), x, y_candidate), jpg_im.ravel()) / np.prod(tif_im.shape) \
            + distance.euclidean(y_candidate.clip(0, 255), y_candidate) * 500

        min_result = minimize(loss, y, method='Powell')

        y_singles.append(min_result.x)

    y_singles = np.array(y_singles)

    y_new = np.median(y_singles, axis=0)

    # Minimize over all images the same time
    loss_all = lambda y_candidate: \
              distance.euclidean(np.interp(tif_channel_pixels.ravel(), x, y_candidate), jpg_channel_pixels.ravel()) / np.prod(tif_channel_pixels.shape) \
            + distance.euclidean(y_candidate.clip(0, 255), y_candidate) * 500

    min_result = minimize(loss_all, y_new, method='Powell')

    y_all = min_result.x

    if plot_results:
        # Define tif2jpg function
        tif2jpg_all = lambda im: np.interp(im, x, y_all)
        tif2jpg_new = lambda im: np.interp(im, x, y_new)
        tif2jpg_per = lambda im: np.interp(im, x, y)

        # Define some aproximation functions (needs more experimentations)
        log_func = lambda t,a,b: a * numpy.log(t) + b
        sqr_func = lambda t,a,b,c: a * t**2 + b * t + c

        log_func_params = curve_fit(log_func,  x,  y_all)[0]
        sqr_func_params = curve_fit(sqr_func,  x,  y_all)[0]

        plt.plot(x, y, '.', label="Percentile match")
        plt.plot(x, y_all, 'o', label="Fitted points overall")
        plt.plot(x, y_new, 'o', label="Fitted points singles")
        plt.plot(x, log_func(x, *log_func_params), label="Log approximation")
        plt.plot(x, sqr_func(x, *sqr_func_params), label="Square approximation")
        plt.legend(loc='lower right')

        jpg_test_imges = jpg_data[choosen_files_idx,...,2-channel].reshape(-1, im_side // downscale_factor).astype(uint8)
        tif_test_imges = tif_data[choosen_files_idx,...,channel].reshape(-1, im_side // downscale_factor)

        fig, ax = plt.subplots(1, 6, sharex=True, sharey=True)

        ax[0].imshow(jpg_test_imges, cmap='gray', vmin=0, vmax=255)
        ax[0].set_title("JPG")

        ax[1].imshow(to_uint8(tif2jpg_per(tif_test_imges)), cmap='gray', vmin=0, vmax=255)
        ax[1].set_title("Percentile match")

        ax[2].imshow(to_uint8(tif2jpg_all(tif_test_imges)), cmap='gray', vmin=0, vmax=255)
        ax[2].set_title("Adjusted overall")

        ax[3].imshow(to_uint8(tif2jpg_new(tif_test_imges)), cmap='gray', vmin=0, vmax=255)
        ax[3].set_title("Adjusted singles")

        ax[4].imshow(to_uint8(log_func(tif_test_imges, *log_func_params)), cmap='gray', vmin=0, vmax=255)
        ax[4].set_title("Log approximation")

        ax[5].imshow(to_uint8(sqr_func(tif_test_imges, *sqr_func_params)), cmap='gray', vmin=0, vmax=255)
        ax[5].set_title("Square approximation")

    funcs_per_channel.append([y_new, y_all, log_func_params, sqr_func_params])

colors = ['red', 'green', 'blue']

for c, y_results in enumerate(funcs_per_channel):
    plt.plot(x_per_channel[c], y_per_channel[2-c], 'o-', color=colors[c], label=colors[c])

plt.legend()

tif_test_imges = tif_data[choosen_files_idx]
jpg_test_imges = jpg_data[choosen_files_idx]

# tif2jpg = lambda im: np.dstack([np.interp(im[...,c], x, my_y[1]) for c, my_y in enumerate(funcs_per_channel)]).clip(0,255).astype(np.uint8)
tif2jpg = lambda im: np.dstack([np.interp(im[...,c], x_per_channel[c], y_per_channel[2-c]) for c in xrange(3)]).clip(0,255).astype(np.uint8)

# choosen_files_idx = []
# for i, f in enumerate(jpg_files):
#     cv2.imshow(f, cv2.imread(f))

#     r = cv2.waitKey()

#     if r == ord('y'):
#         choosen_files_idx.append(i)
#         print(f, r)

#     cv2.destroyWindow(f)

fig, ax = plt.subplots(2, 1)
ax[0].hist(jpg_data[...,0].ravel(), label='jpg0', bins=50, color='red', histtype='step', range=np.percentile(jpg_data.ravel(), [5, 95]), cumulative=True)
ax[0].hist(jpg_data[...,1].ravel(), label='jpg1', bins=50, color='green', histtype='step', range=np.percentile(jpg_data.ravel(), [5, 95]), cumulative=True)
ax[0].hist(jpg_data[...,2].ravel(), label='jpg1', bins=50, color='blue', histtype='step', range=np.percentile(jpg_data.ravel(), [5, 95]), cumulative=True)


ax[1].hist(tif_data[...,0].ravel(), label='tif0', bins=50, color='red', histtype='step', range=np.percentile(tif_data.ravel(), [5, 95]), cumulative=True)
ax[1].hist(tif_data[...,1].ravel(), label='tif1', bins=50, color='green', histtype='step', range=np.percentile(tif_data.ravel(), [5, 95]), cumulative=True)
ax[1].hist(tif_data[...,2].ravel(), label='tif1', bins=50, color='blue', histtype='step', range=np.percentile(tif_data.ravel(), [5, 95]), cumulative=True)

