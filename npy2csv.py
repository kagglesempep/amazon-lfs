from __future__ import print_function
import pandas as pd
import numpy as np
import fnmatch
from sklearn.model_selection import KFold
import os
import re
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--dataroot', required=True, help='path to root of datasets')
parser.add_argument('--output_dir', default='./data/', help='folder where to output csv files')

opt = parser.parse_args()
print(opt)

all_labels = ['agriculture', 'artisinal_mine', 'bare_ground', 'blooming',
       'blow_down', 'clear', 'cloudy', 'conventional_mine', 'cultivation',
       'habitation', 'haze', 'partly_cloudy', 'primary', 'road',
       'selective_logging', 'slash_burn', 'water']

round_digits = 6

y_oof_files = []
for root, dirnames, filenames in os.walk(opt.dataroot):
    for filename in fnmatch.filter(filenames, '*oof*_*pos*.npy'):
        y_oof_files.append((root, filename))
        

for folder, y_oof_file in y_oof_files:
    match_file = re.match(r'y_oof_(\d)pos\.npy', y_oof_file)
    try:
        if match_file:
            print("Converting {}...".format(folder))
            n_pos = int(match_file.group(1))
            y_test_file = y_oof_file.replace('oof', 'test')

            y_oof = np.load(os.path.join(folder, y_oof_file))
            y_test = np.load(os.path.join(folder, y_test_file))

            df_train = pd.read_csv('../input/train_v2.csv', usecols=['image_name'])
            df_sub = pd.read_csv('../input/sample_submission_v2.csv', usecols=['image_name'])

            assert np.all(df_train.index.values == np.arange(df_train.shape[0]))  # Ensure index is sequential for fold matching


            assert y_oof.ndim == 3      # Shape like (40479, 8, 17)
            assert y_test.ndim == 4     # Shape like (4, 61191, 8, 17)

            assert y_oof.shape[0] == df_train.shape[0]
            assert y_test.shape[1] == df_sub.shape[0]

            n_folds = y_test.shape[0]
            kf = KFold(n_folds, random_state=42).split(df_train)

            # Replicate test for each fold
            df_test = [df_sub.copy() for i in xrange(n_folds)]

            # Load classes files, for models with different classes than all_labels
            classes_file = os.path.join(folder, 'CLASSES')

            if os.path.exists(classes_file):
                with open(classes_file) as f:
                    model_labels = f.readlines()
                    model_labels = map(str.strip, model_labels)
            else:
                model_labels = all_labels

            # Convert numpy to dataframe
            for ilabel, label in enumerate(model_labels):
                # Mean
                df_train[label] = y_oof[..., ilabel].mean(axis=1)

                for fold in xrange(n_folds):
                    df_test[fold][label] = y_test[fold, ..., ilabel].mean(axis=1)

                # Min, max and std
                if n_pos > 1:
                    df_train[label + '_min'] = y_oof[..., ilabel].min(axis=1)
                    df_train[label + '_max'] = y_oof[..., ilabel].max(axis=1)
                    df_train[label + '_std'] = y_oof[..., ilabel].std(axis=1)

                    for fold in xrange(n_folds):
                        df_test[fold][label + '_min'] = y_test[fold, ..., ilabel].min(axis=1)
                        df_test[fold][label + '_max'] = y_test[fold, ..., ilabel].max(axis=1)
                        df_test[fold][label + '_std'] = y_test[fold, ..., ilabel].std(axis=1)

            df_train['fold'] = -1
            for ifold, (train, val) in enumerate(kf):
                df_train.loc[val, 'fold'] = ifold

            assert df_train['fold'].min() == 0

            cols_order = list(model_labels)
            if n_pos > 1:
                cols_order.extend(map(lambda l: l + '_std', model_labels))
                cols_order.extend(map(lambda l: l + '_min', model_labels))
                cols_order.extend(map(lambda l: l + '_max', model_labels))

            model_folder = os.path.split(folder)[-1]
            out_file_train = os.path.join(opt.output_dir, '{}_train_{}-folds_{}-pos.csv.bz2'.format(model_folder, n_folds, n_pos))

            df_train[['image_name', 'fold'] + cols_order].round(round_digits).to_csv(out_file_train, index=False, compression='bz2')
            
            for fold in xrange(n_folds):
                out_file_test = os.path.join(opt.output_dir, '{}_test_{}-folds_{}-pos_fold-{}.csv.bz2'.format(model_folder, n_folds, n_pos, fold))
                df_test[fold][['image_name'] + cols_order].round(round_digits).to_csv(out_file_test, index=False, compression='bz2')

        else:
            print("File does not match: {}".format(y_oof_file))
    except AssertionError as e:
        print(e)
