from __future__ import print_function
import pandas as pd
import numpy as np
from os.path import join, exists, split
import glob
import sys
from scipy.optimize import minimize
from sklearn.metrics import fbeta_score
import xgboost as xgb
from util import f2_score, XGBCVHolder
from sklearn.model_selection import StratifiedKFold
import re

n_folds = 4
input_folder = join('..', 'input')
use_extra = False

# if len(sys.argv) < 3:
#     print("Please specify models folders as parameter")
#     sys.exit(1)

#models = sys.argv[1:]

models_num = [7, 10, 11, 14, 15, 21]
models = map(lambda n: 'models/model_%04d' % n, models_num)


for m in models:
    if not exists(m):
        print("Folder '{}' does not exists".format(m))
        sys.exit(2)

all_labels = ['agriculture', 'artisinal_mine', 'bare_ground', 'blooming',
       'blow_down', 'clear', 'cloudy', 'conventional_mine', 'cultivation',
       'habitation', 'haze', 'partly_cloudy', 'primary', 'road',
       'selective_logging', 'slash_burn', 'water']

df_all = pd.read_csv('../input/train_v2.csv')
df_all = pd.concat([df_all['image_name'], df_all.tags.str.get_dummies(sep=' ')], axis=1)
y_true = df_all[all_labels].values.astype('float')


get_model_csv = lambda f: glob.glob(join(f, 'model_*.csv.gz'))[0]
get_fname = lambda f: split(f)[-1]

df_sub = pd.read_csv(join(input_folder, 'sample_submission_v2.csv'))

y_pred = []
y_sub = []

cols_names = []

mean_per_model = {}

for m in models:
    print("Loading model {}...".format(m))

    y_oof_fnames = set(map(get_fname, glob.glob(join(m, 'y_oof*.*'))))

    # Remove 'old' format if 'new' is present
    if 'y_oof.npz' in y_oof_fnames and 'y_oof_8pos.npy' in y_oof_fnames:
        y_oof_fnames -= {'y_oof.npz'}

    for y_oof_fname in y_oof_fnames:
        # 'Old' format
        y_oof_file = join(m, y_oof_fname)
        if y_oof_fname == 'y_oof.npz':
            y_oof = np.load(y_oof_file)

            mean_per_model[y_oof_file] = y_oof['y_pred']

            y_pred.append(y_oof['y_pred'])
            y_sub.append(pd.read_csv(get_model_csv(m), usecols=all_labels).values)

            cols_names.extend([y_oof_file + '_' + l for l in all_labels])

        else:
            print("> Using 8pos data")
            y_oof_8pos = np.load(y_oof_file)
            y_test_8pos = np.load(y_oof_file.replace('oof', 'test'))

            if y_oof_8pos.shape[-1] == 17:
                cols_names.extend([y_oof_file + '_' + l + '_8pos_mean' for l in all_labels])
                cols_names.extend([y_oof_file + '_' + l + '_8pos_std' for l in all_labels])
                mean_per_model[y_oof_file] = y_oof_8pos.mean(axis=-2)
            else:
                label_name = re.findall(r'y_oof_8pos_([\w_]+)\.npy', y_oof_fname)[0]
                cols_names.append(y_oof_file + '_' + label_name + '_single_8pos_mean')
                cols_names.append(y_oof_file + '_' + label_name + '_single_8pos_std')

            # For train, we have shape of (40479, 8, c), where c is the number of classes
            y_pred.append(y_oof_8pos.mean(axis=-2))
            y_pred.append(y_oof_8pos.std(axis=-2))

            # For submission, we have shape (3, 61191, 8, c), that includes the ouput of three folds
            y_sub.append(y_test_8pos.mean(axis=(0,-2)))
            y_sub.append(y_test_8pos.std(axis=-2).mean(axis=0))


# Stack y_pred into X_train and y_sub into X_test
X_train = np.hstack(y_pred)
X_test = np.hstack(y_sub)

def corr_label(idx):
    label = all_labels[idx]

    values = np.hstack([v[:,idx].reshape(-1, 1) for v in mean_per_model.values()])

    model_name = lambda m: re.findall(r'/(model_\d+)', m)[0]

    df_result = pd.DataFrame(columns=map(model_name, mean_per_model.keys()), data=values)

    return df_result.corr()


if use_extra:
    X_train = np.hstack([X_train, np.load('X_train_hist_016.npy')])
    X_test = np.hstack([X_test, np.load('X_test_hist_016.npy')])

assert X_test.shape[0] == df_sub.shape[0], "Test set not in sync"



# Start train-predict for each label

# XGB hyperparameters
xgb_params = {
    'objective': 'binary:logistic',
    'eta': 0.05,
    'max_depth': 3,
    'subsample': 1.0,
    'colsample_bytree': 1.0,
    'silent': 1,
    'eval_metric': 'auc',
    'seed': 42
}

dtest = xgb.DMatrix(X_test)
y_oof = []
y_test = []
for idx, label in enumerate(all_labels):
    print("Tunning label '{}'...".format(label))

    # Perform cross-validation on train data
    dtrain = xgb.DMatrix(X_train, y_true[:, idx])
    folds = list(StratifiedKFold(n_folds, random_state=42).split(dtrain.get_label(), dtrain.get_label()))
    xgb_model = XGBCVHolder(None, dtrain, folds)

    result = xgb.cv(xgb_params, dtrain, num_boost_round=2000, folds=folds, early_stopping_rounds=20, verbose_eval=20, show_stdv=False, callbacks=[xgb_model], seed=42)

    print(result.iloc[-1]['test-auc-mean'])

    # Result of tunning
    num_boost_round = result.shape[0]

    # Get OOF predictions based on models trained on CV
    y_oof.append(xgb_model.predict_oof(ntree_limit=num_boost_round)[1])

    # Train main model
    print("Predicting test...")
    df_sub[label], df_sub[label + '_std'] = xgb_model.predict_mean(dtest, ntree_limit=num_boost_round, include_std=True)



# Join all OOF predictions and y_true
y_oof = np.c_[y_oof].T

cv_f2score = f2_score(y_true > 0.5, y_oof > 0.5)
print("Expected f2 in CV is %.5f\n" % cv_f2score)

# Optimize thresholds
def thres_loss(thresholds):
     result = -f2_score(y_true, y_oof > thresholds)
     print(result, end='\r')
     return result

initial_thresholds = np.array([0.2] * len(all_labels))

# Optimize process
min_result = minimize(thres_loss, initial_thresholds, method='Powell')

print("\nExpected f2 after optimization: %.5f" % min_result.fun)


# Prepare submission
y_pred_thres = df_sub[all_labels].values > min_result.x

df_sub['tags'] = ''
for label, yp in zip(all_labels, y_pred_thres.T):
    df_sub.loc[yp, 'tags'] = df_sub.loc[yp, 'tags'] + ' ' + label

df_sub[['image_name', 'tags']].to_csv('sub_%.5f.csv' % -min_result.fun, index=False)
