from __future__ import print_function

import json
from collections import Counter
from multiprocessing.dummy import Pool, cpu_count
from os.path import exists, join

import cv2
import numpy as np
import pandas as pd
import xgboost as xgb
from scipy import stats
from scipy.optimize import minimize
from sklearn.decomposition import PCA
from sklearn.metrics import fbeta_score
from sklearn.model_selection import StratifiedKFold

from util import SavedModel, XGBCVHolder

try:
    import windows_util
except:
    pass

# Definitions
histogram_size = 16
extra_nn_features = 'resnet50_max'
pca_size = -1
n_folds = 3
image_format = 'jpg' # one of 'jpg' or 'tif'
# min_percentile_features_to_keep = 75
tune_subsample = 1.0
input_folder = join('..', 'input')
train_folder = join(input_folder, 'train-' + image_format)
test_folder = join(input_folder, 'test-' + image_format)
np.random.seed(42)
sm = SavedModel()

# Load data
df_train = pd.read_csv(join(input_folder, 'train_v2.csv'))
df_sub = pd.read_csv(join(input_folder, 'sample_submission_v2.csv'))

# One-hot encoding of 'tags'
df_train = pd.concat([df_train['image_name'], df_train.tags.str.get_dummies(sep=' ')], axis=1)
all_labels = df_train.columns[1:].tolist()

# Util functions
def load_train_im(f):
    return cv2.imread(join(train_folder, f + '.jpg'))

def load_test_im(f):
    return cv2.imread(join(test_folder, f + '.jpg'))

def im2features(im):
    im_size = np.prod(im.shape[:2])
    n_channels = im.shape[2]

    hist_b = cv2.calcHist([im], [0], None, [histogram_size], [0, 256]).ravel() / im_size
    hist_g = cv2.calcHist([im], [1], None, [histogram_size], [0, 256]).ravel() / im_size
    hist_r = cv2.calcHist([im], [2], None, [histogram_size], [0, 256]).ravel() / im_size

    mean_b = im[...,0].mean()
    mean_g = im[...,1].mean()
    mean_r = im[...,2].mean()

    std_b = im[...,0].std()
    std_g = im[...,1].std()
    std_r = im[...,2].std()

    min_b = im[...,0].min()
    min_g = im[...,1].min()
    min_r = im[...,2].min()

    max_b = im[...,0].max()
    max_g = im[...,1].max()
    max_r = im[...,2].max()

    kurt_b = stats.kurtosis(im[..., 0].ravel())
    kurt_g = stats.kurtosis(im[..., 1].ravel())
    kurt_r = stats.kurtosis(im[..., 2].ravel())

    skew_b = stats.skew(im[..., 0].ravel())
    skew_g = stats.skew(im[..., 1].ravel())
    skew_r = stats.skew(im[..., 2].ravel())

    return np.r_[hist_b, hist_g, hist_r, mean_b, mean_g, mean_r, std_b, std_g, std_r,
        min_b, min_g, min_r, max_b, max_g, max_r, kurt_b, kurt_g, kurt_r, skew_b, skew_g, skew_r]

feature_names = ['hist_b_%d' % i for i in xrange(histogram_size)] + \
                 ['hist_g_%d' % i for i in xrange(histogram_size)] + \
                 ['hist_r_%d' % i for i in xrange(histogram_size)] + \
                 ['mean_b', 'mean_g', 'mean_r', 'std_b', 'std_g', 'std_r',
                  'min_b', 'min_g', 'min_r', 'max_b', 'max_g', 'max_r',
                  'kurt_b', 'kurt_g', 'kurt_r', 'skew_b', 'skew_g', 'skew_r']

def train_file2features(f):
    return im2features(load_train_im(f))

def test_file2features(f):
    return im2features(load_test_im(f))

# From here: https://www.kaggle.com/anokas/planet-understanding-the-amazon-from-space/fixed-f2-score-in-python/code
def f2_score(y_true, y_pred):
    y_true, y_pred, = np.array(y_true), np.array(y_pred)
    return fbeta_score(y_true, y_pred, beta=2, average='samples')

# For each sample, we create its histogram
print("Loading features...")
pool = Pool()
try:
    train_cache_file = 'X_train_hist_%03d.npy' % histogram_size
    if exists(train_cache_file):
        X_train = np.load(train_cache_file)
    else:
        print("Creating train images features...")
        hist_train = pool.map(train_file2features, df_train['image_name'])
        X_train = np.array(hist_train)
        np.save(train_cache_file, X_train)

    test_cache_file = 'X_test_hist_%03d.npy' % histogram_size
    if exists(test_cache_file):
        X_test = np.load(test_cache_file)
    else:
        print("Creating test images features...")
        hist_test = pool.map(test_file2features, df_sub['image_name'])
        X_test = np.array(hist_test)
        np.save(test_cache_file, X_test)
finally:
    pool.terminate()

assert X_train.shape[1] == len(feature_names), "Invalid features names"

# Add NN features
print("Extending features with {} data...".format(extra_nn_features))
nn_features_file = np.load('data/{}.npz'.format(extra_nn_features))

X_extra_train = nn_features_file['train']
X_extra_test  = nn_features_file['test']

pca_prefix = ''
if (pca_size > 1) and (pca_size < X_extra_train.shape[1]):
    print("Running PCA to reduce {} features to {}...".format(X_extra_train.shape[1], pca_size))

    pca = PCA(pca_size, random_state=42)
    pca.fit(X_extra_train)
    X_extra_train = pca.transform(X_extra_train)
    X_extra_test  = pca.transform(X_extra_test)

    pca_prefix = 'pca_'

X_train = np.c_[X_train, X_extra_train]
X_test  = np.c_[X_test , X_extra_test]

# Update features names
num_nn_features = X_train.shape[1] - len(feature_names)
feature_names.extend(['{}{}_{}'.format(pca_prefix, extra_nn_features, i) for i in xrange(num_nn_features)])
feature_names = np.array(feature_names)


# XGB hyperparameters
xgb_params = {
    'objective': 'binary:logistic',
    'eta': 0.1,
    'max_depth': 7,
    'subsample': 0.9,
    'colsample_bytree': 0.9,
    'silent': 1,
    'eval_metric': 'auc',
    'seed': 42
}

# Generate dtest (submission)
dtest = xgb.DMatrix(X_test, feature_names=feature_names)

# Subsample (or not)
if tune_subsample < 1:
    idx_train = np.random.choice(len(df_train), int(len(df_train) * tune_subsample))
else:
    idx_train = np.arange(len(df_train))

all_features_scores = Counter()
all_results = []
# Begin CV to find best num_boost_trees
y_true, y_pred = [], []
for label in all_labels:
    print("Tunning label '{}'...".format(label))

    # Perform cross-validation on train data
    dtrain = xgb.DMatrix(X_train[idx_train], df_train.iloc[idx_train][label].values, feature_names=feature_names)
    folds = list(StratifiedKFold(n_folds, random_state=42).split(dtrain.get_label(), dtrain.get_label()))
    xgb_model = XGBCVHolder(feature_names, dtrain, folds)

    result = xgb.cv(xgb_params, dtrain, num_boost_round=2000, folds=folds, early_stopping_rounds=20, verbose_eval=5, show_stdv=False, callbacks=[xgb_model], seed=42)

    all_results.append(result)
    all_features_scores.update(xgb_model.get_fscore())

    print(result.iloc[-1]['test-auc-mean'])

    # Result of tunning
    num_boost_round = result.shape[0]

    # Get OOF predictions based on models trained on CV
    y, y_hat = xgb_model.predict_oof(ntree_limit=num_boost_round)
    y_true.append(y)
    y_pred.append(y_hat)

    # Train main model
    print("Predicting test...")
    df_sub[label], df_sub[label + '_std'] = xgb_model.predict_mean(dtest, ntree_limit=num_boost_round, include_std=True)

    # model.save_model(sm('xgb_{}.model'.format(label)))

# Summary results per label
best_results_per_label = Counter({l:all_results[i].iloc[-1]['test-auc-mean'] for i, l in enumerate(all_labels)}).most_common()

# Join all OOF predictions and y_true
y_true = np.c_[y_true].T
y_pred = np.c_[y_pred].T

np.savez_compressed(sm('y_oof.npz'), y_true=y_true, y_pred=y_pred)

cv_f2score = f2_score(y_true > 0.5, y_pred > 0.5)
print("Expected f2 in CV is %.4f" % cv_f2score)

# Optimize thresholds
def thres_loss(thresholds):
     result = -f2_score(y_true, y_pred > thresholds)
     print(result, thresholds)
     return result

initial_thresholds = np.array([0.5] * len(all_labels))

# Optimize process
min_result = minimize(thres_loss, initial_thresholds, method='Powell')

print("Expected f2 after optimization: %.4f" % min_result.fun)

# Prepare submission
y_pred_thres = df_sub[all_labels].values > min_result.x

df_sub['tags'] = ''
for label, yp in zip(all_labels, y_pred_thres.T):
    df_sub.loc[yp, 'tags'] = df_sub.loc[yp, 'tags'] + ' ' + label

df_sub[['image_name', 'tags']].to_csv(sm('sub_%.4f.csv' % -min_result.fun), index=False)

# Save info about model built
with sm.open('params.json', 'w') as f:
    all_params = {
        'xgb_params': xgb_params,
        'histogram_size': histogram_size,
        'extra_nn_features': extra_nn_features,
        'pca_size': pca_size,
        'image_format': image_format,
        'tune_subsample': tune_subsample,
        'feature_names': list(feature_names),
        'thresholds': list(min_result.x)
    }
    f.write(json.dumps(all_params, indent=2, sort_keys=True))

df_sub.to_csv(sm('model_%.4f.csv.gz' % -min_result.fun), index=False, compression='gzip')

with sm.open('model_%.4f_result.csv' % -min_result.fun, 'w') as f:
    f.write("label,auc\n")
    for label, score in best_results_per_label:
        f.write("{},{}\n".format(label, score))

print("All files saved to {}".format(sm.folder))